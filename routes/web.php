<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();

Route::post('broadcasting/auth', 'Auth\BroadcastController@authorization');

Route::get('/', 'HomeController@index')->name('home');

Route::post('/createOrder', 'OrderController@createOrder');
Route::post('/deleteOrder', 'OrderController@deleteOrder');
Route::get('/paymentProcess', 'InvoiceController@pending');
Route::get('/paymentSuccess', 'InvoiceController@success');
Route::post('/paymentSuccess', 'InvoiceController@getter');
Route::get('/paymentErr', 'InvoiceController@err');
//Route::get('/orders', 'OrderController@getOrdersByDate');
Route::post('/orders', 'OrderController@getOrdersByMonth');
Route::get('/adminordersbydate', 'OrderController@getAdminOrdersByDate');
Route::get('/about', 'HomeController@about');


Route::get('setting', 'SettingController@index')->name('setting.index');
Route::get('instruments', 'InstrumentController@index')->name('instrument.index');



Route::get('/home', 'HomeController@index');

Route::post('check_datetime', 'OrderController@checkDatetime');

Route::resource('session', 'SessionController');

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth' ], function (){
    Route::get('/', 'AdminController@admin')->middleware('is_admin')->name('admin.index');
    Route::get('/update_orders', 'AdminController@orders')->middleware('is_admin');


    Route::group([ 'prefix' => 'order' ], function (){
        Route::get('/', 'OrderController@index')->name('orders.index');
        Route::get('/{id}', 'OrderController@show')->name('orders.show');
        Route::get('/edit/{id}', 'OrderController@edit')->name('orders.edit');
        Route::put('/update', 'OrderController@update')->name('orders.update');
        Route::get('/delete/{id}', 'OrderController@delete')->name('orders.delete');


    });
    Route::group([ 'prefix' => 'payment' ], function (){
        Route::put('/update', 'InvoiceController@update');
    });



    Route::group(['namespace' => 'Admin'], function (){

        Route::resource('order-time', 'OrderTimeController')->parameters([
            'order-time' => 'id'
        ]);

        Route::resource('order-instrument', 'OrderInstrumentController')->parameters([
            'order-instrument.destroy' => 'id, order_id'
        ]);

        Route::resource('schedules', 'SchedulesController')->parameters([
            'schedules.destroy' => 'id, order_id'
        ]);
    });

    Route::group(['namespace' => 'Admin'], function (){

        /**
         * Main Setting
         */
        Route::get('setting/delete/{id}', 'SettingController@delete')->name('setting.delete');
        Route::resource('setting', 'SettingController');

        /**
         * Category
         */
        Route::resource('category', 'CategoryController');

        /**
         * Hour
         */
        Route::resource('hour', 'HourController');
        Route::get('/hour/{day}/duplicate/{category_id}', 'HourController@duplicate')->name('hour.duplicate');
        Route::post('/hour/copy/{category_id}', 'HourController@copy')->name('hour.copy');

        /**
         * Discount
         */
        Route::resource('discount', 'DiscountController');

        /**
         * Reservation
         */
        Route::resource('reservation', 'ReservationController');

        /**
         * Instrument
         */
        Route::get('instrument/delete/{id}', 'InstrumentController@delete')->name('instrument.delete');
        Route::resource('instrument', 'InstrumentController');

        /**
         * Security
         */
        Route::resource('security', 'SecurityController');

    });


});