<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'API\v1', 'middleware' => 'company:api'], function ($route) {

    /**
     * Schedule
     */
    Route::group(['prefix' => 'schedule'], function (){
        Route::post('clear', 'ScheduleController@clear');
        Route::post('between/{from}/{to}', 'ScheduleController@between');
    });
    Route::resource('schedule', 'ScheduleController');

    /**
     * Order
     */
    Route::group(['prefix' => 'order'], function (){
        Route::post('total', 'OrderController@total');
    });
    Route::resource('order', 'OrderController');

    /**
     * Dashboard
     */
    Route::group(['prefix' => 'dashboard'], function (){
       Route::post('schedule', 'DashboardController@addSchedules');
       Route::post('order-total', 'DashboardController@addSchedules');
    });

    /**
     * Category
     */
    Route::group(['prefix' => 'category'], function(){
    });
    Route::resource('category', 'CategoryController');


    /**
     * Cart
     */
    Route::group(['prefix' => 'customer'], function (){
       Route::get('cart', 'CustomerController@cart');
    });

    /**
     * Testing API
     */
    Route::get('testing', function (){
        return false;
    });
});