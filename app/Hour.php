<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    protected $fillable = [
        'category_id',
        'day',
        'hour',
        'price'
    ];

    /**
     * @var array
     * Localisation Day of Week
     */
    private $DAYS_RU = [
        '1' => "Понедельник",
        '2' => "Вторник",
        '3' => "Среда",
        '4' => "Четверг",
        '5' => "Пятница",
        '6' => "Суббота",
        '0' => "Воскресенье",
    ];

    /**
     * use that together
     * $DAYS_ && LANGS[key]
     * $DAYS_RU, $DAYS_UA etc.
     */
    const LANGS = [
        'ru' => 'RU',
        'ua' => 'UA',
        'us' => 'US'
    ];

    /**
     * @param null $category_id
     * @param string $lang
     * @return array|mixed
     *
     * This function return days without repetition
     */
    public function getDays($category_id = null, $lang = 'ru')
    {
        $name = 'DAYS_'.self::LANGS['ru'];
        $days = $this->{$name};

        if ($category_id){
            foreach (self::byDays($category_id)->toArray() as $item) $days = array_diff_key($days, array_flip($item));;
        }

        return $days;
    }

    /**
     * @return mixed
     *
     * This function return Name of Day
     */
    public function getName()
    {

        return $this->getDays()[$this->day];
    }

    /**
     * @param $category_id
     * @return mixed
     *
     * This function return all Days of Category
     */
    static public function byDays($category_id)
    {
        return self::where('category_id', $category_id)
            ->groupBy(['day'])
            ->orderBy('day')
            ->get(['day']);
    }

    static public function byDaysWithHour($category_id)
    {
        $days = self::byDays($category_id);
        $list = [];
        foreach ($days as $day)
        {
            $hours_of_day = Hour::where('day', $day['day'])
                ->where('category_id', $category_id)
                ->get();

            $list[$day['day']] = $hours_of_day;
        }
        return $list;
    }

    public static function calc($data)
    {
        $totals = [
            'main' => 0.00,
            'sale' => 0.00
        ];

        foreach ($data['schedules'] as $day)
        {
            foreach ($day['hours'] as $time){
                $h = self::where('day', $day['day'])
                    ->where('category_id', $day['category_id'])
                    ->where('hour', $time)
                    ->first();

                $totals['main'] += $h->price;
            }

        }

        return $totals;

    }
}
