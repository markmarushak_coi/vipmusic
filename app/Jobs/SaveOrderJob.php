<?php

namespace App\Jobs;

use App\Events\DashboardEvent;
use App\Events\UserMessagesEvent;
use App\Events\UserCartEvent;
use App\Order;
use App\Repository\MainRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SaveOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request;

    private $order;

    private $cart;

    private $jobRepository;

    private $messages = [];

    private $user_id;

    private $crush = [];

    /**
     * Create a new job instance.
     * @param $order Order
     * @param $request Request
     * @param $repository MainRepository
     * @return void
     */
    public function __construct(Order $order, Request $request, MainRepository $repository){

        $this->cart = $request->all();
        $this->order = $order;
        $this->jobRepository = $repository;
        $this->user_id = Session::getId();

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        // save invoice
        if(!$this->saveInvoice($this->order)){
            UserMessagesEvent::dispatch($this->user_id, $this->messages, $this->crush);
        }


        // save schedules
        if(!$this->saveSchedules($this->order)){
            UserMessagesEvent::dispatch($this->user_id, $this->messages, $this->crush);
        }


        // update cart and send to user
        $user_event = new UserCartEvent($this->user_id, $this->order, $this->jobRepository);


        // updated calendar and send to all
        $dashboard_event = new DashboardEvent(
            $this->cart['selectMonth'],
            $this->jobRepository->calendar->getSchedules(
                date('Y-m-01', strtotime($this->cart['selectMonth'])),
                date('Y-m-31', strtotime($this->cart['selectMonth']))
            ),$this->messages
        );


        // start event calendar
        event($dashboard_event);


        // start event cart
        event($user_event);


    }

    public function fail($exception = null)
    {
        DashboardEvent::dispatch(
            $this->cart['selectMonth'],
            null,
            $exception
        );
    }

    /**
     * @param $order
     * @return bool
     */
    private function saveInvoice(Order $order)
    {
        try {

            if(!isset($order->id))
                return false;

            $invoice_entity = $this->jobRepository->invoice->showByOrder($order->id);



            $invoice = [
                'order_id' => $order->id,
                'total' => $this->cart['amount'],
                'discount' => $this->cart['sale'],
                'status' => 'pending'
            ];

            if(empty($invoice_entity->id)){
                $this->jobRepository->invoice->create($invoice);
            }else{
                $invoice['status'] = $invoice_entity->status; // status don't change to pending
                $this->jobRepository->invoice->update($invoice_entity->id, $invoice);
            }

            return true;
        } catch (\Exception $err){
            $this->jobRepository->invoice->deleteByOrder($order->id);
            return false;
        }
    }

    /**
     * @param $order Order
     * @return bool
     */
    private function saveSchedules(Order $order){

        try{
            $messages = [];

            if(!isset($order->id))
                return false;

            $this->jobRepository->schedules->deleteByOrder($order->id);

            foreach ($this->cart['schedules'] as $hours){
                foreach ($hours['hours'] as $hour){

                    $schedule = [
                        'order_id' => $order->id,
                        'date' => strtotime($hours['date']),
                        'hour' => $hour
                    ];

                    $schedule = $this->jobRepository->schedules->create($schedule);

                    if(!isset($schedule->id)){
                        $this->messages = array_merge($messages, $schedule);
                        $this->crush[$hours['date']][$hour] = $hour;
                    }

                }
            }

            if(count($messages)){
                return false;
            }

            return true;
        }catch (\Exception $err){
            $this->jobRepository->schedules->deleteByOrder($order->id);
            return false;
        }

    }
}
