<?php

namespace App\Jobs;

use App\Events\DashboardEvent;
use App\Events\UserMessagesEvent;
use App\Order;
use App\Repository\CalendarRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PendingOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $order;

    protected $calendar;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->calendar = new CalendarRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->order = Order::find($this->order->id);
        if(!count($this->order->schedules) || $this->order->timeout > time()){
            report(new \Exception('Schedules Null'));
            return;
        }

        if($this->order->payment->status === 'pending'){
            $this->order::destroy($this->order->id);
        }

        $month = null;

        foreach ($this->order->schedules as $schedule){

            $month = date('Y-m', $schedule->date);
            break;

        }

        if(!empty($month)){
            DashboardEvent::dispatch(
                $month,
                $this->calendar->getSchedules(
                    date('Y-m-01', strtotime($month)),
                    date('Y-m-31', strtotime($month))
                )
            );
        }
    }

    public function fail($exception = null)
    {
        UserMessagesEvent::dispatch(Session::getId(), $exception);
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->delete();
    }

}
