<?php

namespace App;

use App\Repository\SchedulesRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class Schedule extends Model
{
    protected $fillable = [
        'order_id',
        'date',
        'hour'
    ];

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'order_id', 'order_id');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'id', 'order_id');
    }

    public function validate(array $data)
    {
        $v = Validator::make($data, [
            'order_id' => 'required',
            'date' => 'required',
            'hour' => [
                'required',
                Rule::unique('schedules')->where(function ($query) use ($data) {
                    return $query->where('date', $data['date'])->where('hour', $data['hour']);
                })
            ],
        ],[
            'order_id.required' => 'Ордер не определен',
            'date.required' => 'Дата не определена',
            'hour.required' => 'Время не определена',
            'hour.unique' => 'Время с ' . $data['hour'] . ' по ' . (1 + $data['hour']) . ' на ' . $data['date'] . ' уже забронированно ',
        ]);

        if ($v->fails()) {

            $errors = $v->errors();

            foreach ($errors->all() as $message){
                SchedulesRepository::addMessage($message);
            }

            return false;
        } else {
            return true;
        }

    }
}
