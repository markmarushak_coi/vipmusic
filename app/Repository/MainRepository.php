<?php

namespace App\Repository;


class MainRepository
{

    public $order;

    public $schedules;

    public $invoice;

    public $calendar;


    /**
     * Create a new job instance.
     * @param $order OrderRepository
     * @param $schedules SchedulesRepository
     * @param $invoice InvoiceRepository
     * @param $calendar CalendarRepository
     * @return void
     */
    public function __construct(
        OrderRepository $order,
        SchedulesRepository $schedules,
        InvoiceRepository $invoice,
        CalendarRepository $calendar
    ){
        $this->order = $order;
        $this->schedules = $schedules;
        $this->invoice = $invoice;
        $this->calendar = $calendar;
    }

}