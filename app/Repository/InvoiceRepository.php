<?php

namespace App\Repository;


use App\Invoice;

class InvoiceRepository
{

    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }


    /**
     * @param $data
     * @return object|bool
     */
    public function create($data)
    {
        if($this->invoice->validate($data)){
            return $this->invoice::create($data);
        }else{
            return false;
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data){

        $invoice = $this->invoice::find($id);
        if($this->invoice->validate($data) && isset($invoice->id)){
            return $invoice->update(array_keys($data), array_values($data));
        }else{
            return false;
        }

    }

    public function show($id)
    {
        $invoice = $this->invoice::find($id);
        if(isset($invoice->id)){
            return $invoice;
        }else{
            return false;
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $invoice = $this->invoice::find($id);
        if(isset($invoice->id)){
            return $this->invoice::destroy($id);
        }else{
            return false;
        }
    }

    public function showByOrder($order_id)
    {
        $invoice = $this->invoice::where('order_id', $order_id)->first();
        if(isset($invoice->id)){
            return $invoice;
        }else{
            return false;
        }
    }

    public function deleteByOrder($order_id)
    {
        $invoices = $this->invoice::where('order_id', $order_id)->get();
        if($invoices->count()){
            foreach ($invoices as $invoice) $invoice->delete();
            return true;
        }else{
            return false;
        }
    }

}