<?php

namespace App\Repository;

use App\Order;
use App\Schedule;

class CalendarRepository
{

    private $orders;

    private $schedules;

    public function __construct()
    {
        $this->orders = new Order();
        $this->schedules = new Schedule();
    }

    /**
     * @param $from string
     * @param $to string
     * @param $status bool
     * @return array | false
     * This function get all schedules around period $from and $to
     */
    public function getSchedules($from, $to)
    {
        if (!empty($from) && !empty($to)) {

            $schedules = $this->schedules
                ->with(['invoice','order'])
                ->where('date', '>=', strtotime($from))
                ->where('date', '<=', strtotime($to))
//                ->whereHas('order', function ($query){
//                    return $query
//                        ->where('timeout','>',time()) // if timeout low than now
//                        ->OrWhere('timeout', 0); // if order don't have booked time
//                })
                ->get();

            $schedules_list = [];
            foreach ($schedules as $schedule){
                $date = date('Y-m-d', $schedule->date);
                $schedules_list[$date][$schedule->hour] = [
                    'date' => $date
                ];

                try{
                    $schedules_list[$date][$schedule->hour]['status'] = $schedule->invoice->status;
                }catch (\Exception $err){
                    $schedules_list[$date][$schedule->hour]['status'] = 'failed';
                }
            }

            return $schedules_list;
        } else {
            return false;
        }
    }

    public function getSchedulesByDay($from, $to)
    {

        if (!empty($from) && !empty($to)) {

            $schedules = $this->schedules
                ->with(['invoice'])
                ->where('date', '>=', strtotime($from))
                ->where('date', '<=', strtotime($to))
                ->get();

            $schedules_list = [];
            foreach ($schedules as $schedule){
                $date = date('Y-m-d', $schedule->date);
                $schedules_list[$date][$schedule->hour] = [
                    'date' => $date
                ];

                try{
                    $schedules_list[$date][$schedule->hour]['status'] = $schedule->invoice->status;
                }catch (\Exception $err){
                    $schedules_list[$date][$schedule->hour]['status'] = 'failed';
                }
            }

            return $schedules_list;
        } else {
            return false;
        }
    }

    /**
     * @param $from
     * @param $to
     * @return array|bool
     *
     * The function
     */
    public function getSchedulesByStatus($from, $to, $status)
    {

        if (!empty($from) && !empty($to)) {

            $schedules = $this->schedules
                ->with(['invoice'])
                ->where('date', '>=', strtotime($from))
                ->where('date', '<=', strtotime($to))
                ->whereHas('invoice', function ($query) use ($status){
                    return $query->where('status', $status);
                })
                ->get();

            $schedules_list = [];
            foreach ($schedules as $schedule){
                $date = date('Y-m-d', $schedule->date);
                $schedules_list[$date][$schedule->hour] = [
                    'status' => $schedule->invoice->status,
                    'date' => $date
                ];
            }

            return $schedules_list;
        } else {
            return false;
        }
    }

    /**
     * @param $schedules
     * @return array
     *
     * The function call immediately after send OrderTotal
     */
    public function toCalendarSchedule($schedules)
    {
        $__schedules = [];

        foreach ($schedules as &$schedule)
        {
            foreach ($schedule['hours'] as $hour){
                $__schedules[$schedule['date']][$hour] = [
                    'status' => 'pending',
                    'date' => $schedule['date'],
                    'day' => date('w', strtotime($schedule['date']))
                ];
            }
        }


        return $__schedules;
    }

    /**
     * @param $order_id
     * @param $category_id
     * @return array
     *
     * The function update schedules in cart
     */
    public function getSchedulesByOrder($order_id, $category_id)
    {

        $schedules = $this->schedules->where('order_id', $order_id)->get();

        $schedules_list = [];

        foreach ($schedules as $schedule) {
            $date = date('Y-m-d', $schedule->date);

            $schedules_list[$date]['hours'][] = $schedule->hour;
            $schedules_list[$date]['date'] = $date;
            $schedules_list[$date]['day'] = date('w', strtotime($date));
            $schedules_list[$date]['category_id'] = $category_id;

        }

        return $schedules_list;

    }


}