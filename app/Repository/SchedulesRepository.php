<?php

namespace App\Repository;


use App\Schedule;

class SchedulesRepository
{

    private $schedule;

    public static $messages = [];

    /**
     * SchedulesRepository constructor.
     * @param Schedule $schedule4
     */
    public function __construct(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * @param $data
     * @return object| array
     */
    public function create($data)
    {
        if($this->schedule->validate($data)){
            return $this->schedule::create($data);
        }else{
            return $this->getMessages();
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data){

        $schedule = $this->schedule::find($id);
        if($this->schedule->validate($data) && isset($schedule->id)){
            return $schedule->update(array_keys($data), array_values($data));
        }else{
            return false;
        }

    }

    public function show($id)
    {
        $schedule = $this->schedule::find($id);
        if(isset($schedule->id)){
            return $schedule;
        }else{
            return false;
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $schedule = $this->schedule::find($id);
        if(isset($schedule->id)){
            return $this->schedule::destroy($id);
        }else{
            return false;
        }
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function showByOrder($order_id)
    {
        $schedule = $this->schedule::where('order_id', $order_id)->first();
        if(isset($schedule->id)){
            return $schedule;
        }else{
            return false;
        }
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function deleteByOrder($order_id)
    {
        $schedules = $this->schedule::where('order_id', $order_id)->get();
        if($schedules->count()){
            foreach ($schedules as $schedule) $schedule->delete();
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function deleteByOrderAndDate($order_id, $date)
    {
        $schedules = $this->schedule::where('order_id', $order_id)
            ->where('date', strtotime($date))
            ->get();

        if($schedules->count()){
            foreach ($schedules as $schedule) $schedule->delete();
            return true;
        }else{
            return false;
        }
    }


    /**
     * @param $message
     * @return mixed
     */
    public static function addMessage($message)
    {
        return self::$messages[] = $message;
    }

    /**
     * @return array
     */
    public function getMessages(){
        return self::$messages;
    }

    /**
     * @param $from string
     * @param $to string
     * @param $status bool
     * @return array | false
     * This function get all schedules around period $from and $to
     */
    public function getSchedules($from, $to)
    {
        if (!empty($from) && !empty($to)) {

            $schedules = $this->schedule
                ->with(['invoice','order'])
                ->where('date', '>=', strtotime($from))
                ->where('date', '<=', strtotime($to))
//                ->whereHas('order', function ($query){
//                    return $query
//                        ->where('timeout','>',time()) // if timeout low than now
//                        ->OrWhere('timeout', 0); // if order don't have booked time
//                })
                ->get();

            $schedules_list = [];
            foreach ($schedules as $schedule){
                $date = date('Y-m-d', $schedule->date);
                $schedules_list[$date][$schedule->hour] = [
                    'date' => $date
                ];

                try{
                    $schedules_list[$date][$schedule->hour]['status'] = $schedule->invoice->status;
                }catch (\Exception $err){
                    $schedules_list[$date][$schedule->hour]['status'] = 'failed';
                }
            }

            return $schedules_list;
        } else {
            return false;
        }
    }

    public function getSchedulesByDay($from, $to)
    {

        if (!empty($from) && !empty($to)) {

            $schedules = $this->schedule
                ->with(['invoice'])
                ->where('date', '>=', strtotime($from))
                ->where('date', '<=', strtotime($to))
                ->get();

            $schedules_list = [];
            foreach ($schedules as $schedule){
                $date = date('Y-m-d', $schedule->date);
                $schedules_list[$date][$schedule->hour] = [
                    'date' => $date
                ];

                try{
                    $schedules_list[$date][$schedule->hour]['status'] = $schedule->invoice->status;
                }catch (\Exception $err){
                    $schedules_list[$date][$schedule->hour]['status'] = 'failed';
                }
            }

            return $schedules_list;
        } else {
            return false;
        }
    }

    /**
     * @param $from
     * @param $to
     * @return array|bool
     *
     * The function
     */
    public function getSchedulesByStatus($from, $to, $status)
    {

        if (!empty($from) && !empty($to)) {

            $schedules = $this->schedule
                ->with(['invoice'])
                ->where('date', '>=', strtotime($from))
                ->where('date', '<=', strtotime($to))
                ->whereHas('invoice', function ($query) use ($status){
                    return $query->where('status', $status);
                })
                ->get();

            $schedules_list = [];
            foreach ($schedules as $schedule){
                $date = date('Y-m-d', $schedule->date);
                $schedules_list[$date][$schedule->hour] = [
                    'status' => $schedule->invoice->status,
                    'date' => $date
                ];
            }

            return $schedules_list;
        } else {
            return false;
        }
    }

    /**
     * @param $schedules
     * @return array
     *
     * The function call immediately after send OrderTotal
     */
    public function toCalendarSchedule($schedules)
    {
        $__schedules = [];

        foreach ($schedules as &$schedule)
        {
            foreach ($schedule['hours'] as $hour){
                $__schedules[$schedule['date']][$hour] = [
                    'status' => 'pending',
                    'date' => $schedule['date'],
                    'day' => date('w', strtotime($schedule['date']))
                ];
            }
        }


        return $__schedules;
    }

    /**
     * @param $order_id
     * @param $category_id
     * @return array
     *
     * The function update schedules in cart
     */
    public function getSchedulesByOrder($order_id, $category_id)
    {

        $schedules = $this->schedule->where('order_id', $order_id)->get();

        $schedules_list = [];

        foreach ($schedules as $schedule) {
            $date = date('Y-m-d', $schedule->date);

            $schedules_list[$date]['hours'][] = $schedule->hour;
            $schedules_list[$date]['date'] = $date;
            $schedules_list[$date]['day'] = date('w', strtotime($date));
            $schedules_list[$date]['category_id'] = $category_id;

        }

        return $schedules_list;

    }
}