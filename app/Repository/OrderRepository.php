<?php

namespace App\Repository;

use App\Discount;
use App\Hour;
use App\Order;
use App\Reservation;

class OrderRepository
{

    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $data
     * @return object|bool
     */
    public function create($data)
    {
        if($this->order->validate($data)){
            return $this->order::create($data);
        }else{
            return false;
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($order, $data){

        if($this->order->validate($data) && isset($order->id)){
            return $order->update(array_keys($data), array_values($data));
        }else{
            return false;
        }

    }

    public function show($id)
    {
        $order = $this->order::find($id);
        if(isset($order->id)){
            return $order;
        }else{
            return false;
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $order = $this->order::find($id);
        if(isset($order->id)){
            return $this->order::destroy($id);
        }else{
            return false;
        }
    }

    /**
     * The function calculate by own formula
     * - category
     * - date
     * - hours
     * = total
     *
     * @param array $data
     * @param $category_id
     * @return array
     */
    public function total(array $data, $category_id)
    {
        $total = [];
        $messages = [];
        if(
            $data['reservation'] === true &&
            Reservation::findSale($category_id)
        ){
            try{
                $total = Reservation::calc($category_id,$data);
            }catch(\Exception $err){
                $messages[] = $err->getMessage();
            }
        }else if(Discount::findSale($category_id)){
            try{
                $total = Discount::calc($category_id, $data);
            }catch(\Exception $err){
                $messages[] = $err->getMessage();
            }
        }else{
            try{
                $total = Hour::calc($data);
            }catch(\Exception $err){
                $messages[] = $err->getMessage();
            }
        }

        if(count($messages) === 0){
            array_push($messages, 'Ордер успешно посчитан');
        }else{
            return [
                'status' => 'error',
                'messages' => $messages
            ];
        }

        return [
            'status' => 'success',
            'amount' => $total['main'],
            'sale' => $total['sale'],
            'messages' => $messages,

        ];
    }

}