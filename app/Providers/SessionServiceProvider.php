<?php

namespace App\Providers;

use App\Extensions\DatabaseHandler;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class SessionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Session::extend('DatabaseHandler', function ($app){
            return new DatabaseHandler;
        });
    }
}
