<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderInstrument;
use App\Invoice;
use App\Schedule;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OrderController extends Controller
{

    public function createOrder(Request $request)
    {



        $month_id = 0;
        $amount_total = floatval(0);
        $admin = false;

        foreach ($request->all() as $data) {

            /**
             *  PendingOrderJob
             */
            $admin = isset($data['admin']) ? $data['admin'] : false ;

            $order = new Order();
            $order->name = $data['name'];
            $order->group_name = empty($data['group']) ? '' : $data['group'];
            $order->phone = $data['phone'];
            $order->email = $data['email'];
            $order->callback = intval($data['callback']);
            $order->type = $data['type'];
            $order->month = $month_id;
            $order->save(   );

            $amount_total += $data['amount'];

            if(!$month_id){
                $month_id =  $order->hash = 'VIP_' . rand();
                $order->save();
            }



            /**
             * Schedule
             */
            foreach ($data['time'] as $start) {
                $schedules = new Schedule();
                $schedules->order_id = $order->id;
                $schedules->start = $start;
                $schedules->date = strtotime($data['date']);
                $schedules->save();
            }

            /**
             * Instruments
             */
            if (isset($data['instruments'])) {
                foreach ($data['instruments'] as $instrument) {
                    $order_instrument = new OrderInstrument();
                    $order_instrument->order_id = $order->id;
                    $order_instrument->instrument_id = $instrument;
                    $order_instrument->save();
                }
            }

            /**
             * Invoice
             */
            $payment = new Invoice();
            $payment->order_id = $order->id;
            $payment->invoice_id = 0;
            $payment->amount = $data['amount'];
            $payment->sale = $data['sale'];
            $payment->status = 'pending';
            $payment->save();
        }

        if( empty(auth()->user()->isAdmin()) && $admin ){

            $payment = new InvoiceController();
            session(['ik_pm_no' => $month_id]);
            $payment->success($request);

            return response()->json([
                'status' => 'success'
            ]);

        }

        $form = view('form-pay', [
            'order_id' => $month_id,
            'amount' => $amount_total,
            'name' => $order->name
        ])->render();

        if ($order->id && $schedules->id && $payment->id) {
            return response()->json([
                'form' => $form,
                'order_id' => $month_id,
                'amount' => $amount_total,
                'name' => $order->name
            ]);
        } else {
            Invoice::where('order_id', $order->id)->delete();
            OrderInstrument::where('order_id', $order->id)->delete();
            Schedule::where('order_id', $order->id)->delete();
            Order::where('id', $order->id)->delete();
        }
    }

    public function getOrdersByDate(Request $request)
    {
        if ($request->date) {
            $date = strtotime($request->date);
            $activeOrders = Order::whereHas('schedules', function ($query) use ($date) {
                $query->where('date', $date);
            })->get();

            $takenHours = [];
            $key = 9;

            foreach ($activeOrders as $order) {
                foreach ($order->schedules as $schedule) {
                    $schedule->status = $order->payment->status;
                    $takenHours[$key] = $schedule;
                    $key++;
                }
            }

            return response()->json($takenHours);
        } else {
            return false;
        }
    }

    public function getOrdersByMonth(Request $request)
    {
        $alarm = false;
        $total = count(Order::all());
         if(!empty(session('orders'))){
            if(session('orders') != $total){
                $alarm = true;
                session(['orders' => $total]);
            }
         }else{
            session(['orders' => $total]);
         }

        if ($request->prev && $request->next) {
            $orders = Order::whereHas('schedules', function ($query) use ($request) {
                $query->where('date', '>', strtotime($request->prev));
                $query->where('date', '<', strtotime($request->next));
            })->get();
            $schedules = [];
            foreach ($orders as &$order){
                foreach ($order->schedules as &$schedule){
                    $schedule->date = date('Y-m-d', $schedule->date);
                    $schedules[$schedule->date][] = $schedule->start;
                }
                $order->payment->status;
            }

            return response()->json([
                'orders' => $orders,
                'schedules' => $schedules,
                'alarm' => $alarm
            ]);
        } else {
            return false;
        }
    }

    public function checkDatetime(Request $request)
    {
        if ($request->dates && $request->time) {
            $error = [];
            $valid = [];
            foreach ($request->dates as $key => $date) {
                $date_unix = strtotime($date);
                foreach ($request->time as $time) {
                    $datetime = Schedule::where('date', $date_unix)
                        ->where('start', $time)->first();
                    if ($datetime) {
                        $error[] = [
                            'date' => $date,
                            'time' => $time
                        ];
                    } else {
                        $valid[$key]['date'] = $date;
                        $valid[$key]['time'][] = $time;
                    }
                }
            }


            return response()->json([
                'error' => $error,
                'valid' => $valid,
            ]);
        }
    }


    public function getAdminOrdersByDate(Request $request)
    {
        if ($request->date) {
            $date = strtotime($request->date);
            $activeOrders = Order::whereHas('schedules', function ($query) use ($date) {
                $query->where('date', $date);
            })->get();

            $takenHours = [];
            $key = 9;

            foreach ($activeOrders as $order) {
                foreach ($order->schedules as $schedule) {
                    $schedule->status = $order->payment->status;
                    $schedule->name = $order->name;
                    $schedule->type = $order->type;
                    $schedule->callback = $order->callback;
                    $schedule->amount = $order->amount;
                    $takenHours[$key] = $schedule;
                    $key++;
                }
            }

            return response()->json($takenHours);
        } else {
            return false;
        }
    }

    public function deleteOrder($id = NULL)
    {
        DB::table('orders')->where('id', request()->all()['orderid'])->update(['status' => 0]);
        return request()->all()['orderid'];
    }

    public function index()
    {
        return view('admin.order.index', [
            'orders' => Order::all()
        ]);
    }

    public function edit(Request $request)
    {

        $order = Order::find($request->id);

        return view('admin.order.edit', [
            'order' => $order
        ]);
    }

    public function update(Request $request){

        Order::where('id', $request->order['id'])
            ->update([$request->key => $request->order[$request->key]]);

        return response()->json([$request->key => $request->order[$request->key]]);
    }

    public function show(Request $request)
    {
        $orders = Order::with(['schedules', 'payment', 'instruments'])->whereHas('schedules', function ($query) {
            $query->where('date', strtotime(\request()->get('date')));
        })->get();

        foreach ($orders as &$order){
            foreach ($order->instruments as &$instrument){
                $instrument = $instrument->instrument;
            }
        }

        if($request->json){
            return response()->json([
               'orders' => $orders
            ]);
        }else{
            return view('admin.order.index', [
                'orders' => $orders
            ]);
        }

    }

    public function delete(Request $request)
    {
        Invoice::where('order_id', $request->id)->delete();
        OrderInstrument::where('order_id', $request->id)->delete();
        Schedule::where('order_id', $request->id)->delete();
        Order::where('id', $request->id)->delete();
        return redirect()->back();
    }
}
