<?php

namespace App\Http\Controllers;

use App\Instrument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class InstrumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\request()->get('type') == 'json'){
            return response()->json(Instrument::all());
        }else{
            return view('admin.instrument.index', [
                'instruments' => Instrument::all()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.instrument.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('img');
        $extension = $image->getClientOriginalExtension();
        Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
        $instrument = new Instrument();
        $instrument->name = $request->name;
        $instrument->img = '/public/storage/' . $image->getFilename() . '.' . $extension;
        $instrument->save();

        return redirect()->route('instrument.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instrument = Instrument::find($id);
        return view('admin.instrument.edit', [
            'instrument' => $instrument
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instrument = Instrument::find($id);

        $old_img = str_replace('/public/storage/', '', $instrument->img);

        $image = $request->file('img');
        $extension = $image->getClientOriginalExtension();
        $img = Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));

        if($img){
            unlink('storage/app/public/' . $old_img);
        }

        $instrument->name = $request->name;
        $instrument->img = '/public/storage/' . $image->getFilename() . '.' . $extension;;
        $instrument->save();
        return redirect()->route('instrument.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $instrument = Instrument::find($id);
        if(File::delete($instrument->img)){
            $instrument->delete();
        }
        return redirect()->back();
    }
}
