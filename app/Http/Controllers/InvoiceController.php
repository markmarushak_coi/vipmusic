<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Order;
use App\User;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{

	function pending(){
        if (request()->query()['ik_pm_no']){
            $payment= new \App\Invoice;
            $payment->paymentid = request()->query()['ik_pm_no'];
            $payment->orderid = request()->query()['ik_x_paidorderid'];
            $payment->amount = request()->query()['ik_am'];
            $payment->status = 1;
            $payment->save();
            $updatedOrder=\App\Order::all()[intval(request()->query()['ik_x_paidorderid'])-1];
            $updatedOrder->amount_paid=request()->query()['ik_am'];
            $updatedOrder->ik_pay_id=request()->query()['ik_pm_no'];
            $updatedOrder->status=1;
            $updatedOrder->save();

        }
		// return view('paymentProcess');
	}

	function success(Request $request) {

        $invoice = null;
        $ik_pm_no = null;

        if(auth()->user()->isAdmin()){
            $ik_pm_no = session('ik_pm_no');
            $invoice = 0;
        }else{
            $invoice = $request->ik_inv_id;
            $ik_pm_no = $request->ik_pm_no;
        }


        $payments = Order::with(['payment'])
            ->where('month', $ik_pm_no)
            ->orWhere('hash', $ik_pm_no)
            ->get();

        foreach ($payments as $payment){
            $payment->payment->invoice_id = $invoice;
            $payment->payment->status = 'success';
            $payment->payment->save();
        }

        return view('paymentSuccess');
	}

    function err(Request $request) {

        $orders = Order::with(['payment', 'schedules', 'instruments'])
            ->where('month', $request->ik_pm_no)
            ->orWhere('hash', $request->ik_pm_no)
            ->get();

        foreach ($orders as $order){
            foreach ($order->payments as $payment){
                $payment->delete();
            }
            foreach ($order->instruments as $instrument){
                $instrument->delete();
            }
            foreach ($order->schedules as $schedule){
                $schedule->delete();
            }

            $order->delete();
        }

    	return view('paymentErr');
	}

    public function update(Request $request){

        Invoice::where('order_id', $request->order['id'])
            ->update([$request->key => $request->order['payment'][$request->key]]);

        return response()->json([$request->key => $request->order['payment'][$request->key]]);
    }
}
