<?php

namespace App\Http\Controllers;

use App\Instrument;
use App\Order;
use App\Setting;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {

        $settings = Setting::all()->toJson();
        $instruments = Instrument::all()->toJson();
//        $orders = PendingOrderJob::with(['schedules', 'payment', 'instruments'])->get();

        return view('adminlayout', [
            'settings' => $settings,
            'instruments' => $instruments
        ]);
    }
    public function orders()
    {
        return Order::with(['schedules', 'payment', 'instruments'])->get();
    }


}
