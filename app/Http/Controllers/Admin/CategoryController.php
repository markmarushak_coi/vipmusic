<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Hour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new Category();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index', [
           'categories' => $this->model->with(['parent'])->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create', [
            'categories' => $this->model->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => [
                'required',
                'unique:'.$this->model->getTable().',name',
            ],
            'sub_name' => [
                'required',
                'unique:'.$this->model->getTable().',sub_name'
            ],
            'min_hour' => 'required|min:1',
        ], [], $request->all());

        $category = $this->model::create($request->all());

        if(isset($category->id)){
            return redirect()->route('category.edit', [
                'id' => $category->id
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->model::find($id);


        $days = Hour::byDays($id);

        $discounts = $category->discounts;

        $reservations = $category->reservations;

        return view('admin.category.edit', [
            'id' => $id,
            'category' => $category,
            'categories' => $this->model->where('id','!=', $id)->get(),
            'days' => $days,
            'discounts' => $discounts,
            'reservations' => $reservations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique($this->model->getTable())->ignore($id)
            ],
            'sub_name' => [
                'required',
                Rule::unique($this->model->getTable())->ignore($id)
            ],
            'min_hour' => 'required|min:1',
        ], [], $request->all());

        $category = $this->model::find($id);

        if(!isset($category->id)){
            return redirect()->back();
        }

        $category->name = $request->name;
        $category->sub_name = $request->sub_name;
        $category->min_hour = $request->min_hour;

        if(isset($request->parent_id) && $request->parent_id){
            $category->parent_id = $request->parent_id;
        }

        $category->save();

        if(isset($category->id)){
            return redirect()->route('category.edit', [
                'id' => $id,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model::destroy($id);
        return redirect()->back();
    }
}
