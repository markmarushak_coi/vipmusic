<?php

namespace App\Http\Controllers\Admin;

use App\Schedule;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class OrderTimeController extends Controller
{
    protected $start_work;
    protected $finish_work;
    protected $free_times = [];
    protected $use = [];
    public function __construct(Request $request)
    {
        if(!isset($request->date)){
            abort(404);
        }

        $this->start_work = $this->settings('start_work');
        $this->finish_work = $this->settings('finish_work');

        Schedule::where('date', $request->date)->get()->map(function ($sch){
            array_push($this->use, $sch->start);
        });

        for ($time = $this->start_work; $time <= $this->finish_work; $time++){
            if(in_array($time, $this->use)){
                continue;
            }else{
                array_push($this->free_times, $time);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return response()->json($this->free_times);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $time)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
