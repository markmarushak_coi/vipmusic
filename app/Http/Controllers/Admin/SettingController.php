<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return view('admin.setting.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|unique:settings|max:255',
            'value' => 'required',
        ]);

        if(substr($request->name,0,1) == '_'){
            $setting = new Setting();
            $setting->name = 'min_hours_' . $request->value;
            $setting->value = 0;
            $setting->save();

            $setting = new Setting();
            $setting->name = 'sale_' . $request->value;
            $setting->value = 0;
            $setting->save();
        }

        $setting = new Setting();
        $setting->name = $request->name;
        $setting->value = $request->value;
        $setting->save();


        return redirect()->route('setting.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        return view('admin.setting.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        $disabled_name = false;
        \session()->put('old', $setting);

        foreach (config('app.disabled_name') as $name){
            if(strpos($setting->name, $name) === false){
                $disabled_name = false;
            }else{
                $disabled_name = true;
                break;
            }
        }

        return view('admin.setting.edit', [
            'setting' => $setting,
            'disabled_name' => $disabled_name
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $request->validate([
            'value' => 'required|max:255',
        ]);

        if(isset($request->name))
            if(substr($request->name,0,1) == '_'){
                $old = session()->get('old');
                Setting::where('name', 'min_hours_' . $old->value)->update(['name' => 'min_hours_' . $request->value]);
                Setting::where('name', 'sale_' . $old->value)->update(['name' => 'sale_' . $request->value]);
            }

        isset($request->name) ? $setting->name = $request->name: false;
        $setting->value = $request->value;
        $setting->save();

        return redirect()->route('setting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function delete(Setting $setting, Request $request)
    {

        if(substr($request->id,0,1) == '_'){
            $parent = $setting::where('name', $request->id)->first();
            Setting::where('name', 'min_hours_' . $parent->value)->delete();
            Setting::where('name', 'sale_' . $parent->value)->delete();
        }
        $setting::where('name', $request->id)->delete();
        return redirect()->back();
    }

}
