<?php

namespace App\Http\Controllers\Admin;

use App\Instrument;
use App\OrderInstrument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderInstrumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order_instruments = [];
        $result = [];
        $instruments = Instrument::all();

        if(isset($request->order_id)){
            $order_instruments = OrderInstrument::where('order_id', $request->order_id)->get();
        }

        foreach ($instruments as $instrument){
            foreach ($order_instruments as $order_instrument){
                if($order_instrument->instrument_id == $instrument->id){
                    continue;
                }else{
                    array_push($result, $instrument);
                }
            }
        }

        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order_instrumetn = new OrderInstrument();
        $order_instrumetn->order_id = $request->id;
        $order_instrumetn->instrument_id = $request->instrument;
        $order_instrumetn->save();

        return response()->json(
            OrderInstrument::with(['instrument'])
                ->where('order_id', $request->order_id)
                ->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
