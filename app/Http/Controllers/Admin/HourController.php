<?php

namespace App\Http\Controllers\Admin;

use App\Hour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class HourController extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new Hour();

        if(\request()->get('category_id')){
            $this->category_id = \request()->get('category_id');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(request()->get('category_id')){
            $category_id = request()->get('category_id');
        }else{
            return redirect()->back();
        }

        $hours = $selected_day = null;

        if(request()->get('hours')){
            $hours = request()->get('hours');
        }

        if(request()->get('selected_day')){
            $selected_day = request()->get('selected_day');
        }

        return view('admin.hour.create',[
            'days' => $this->model->getDays($category_id),
            'selected_day' => $selected_day,
            'hours' => $hours,
            'category_id' => $category_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        foreach ($data['data'] as $hour)
        {
            $hour['day'] = $request->day;

            $this->model::firstOrCreate([
                'category_id' => $hour['category_id'],
                'day' => $hour['day'],
                'hour' => $hour['hour'],
            ],$hour);
        }

        return redirect()->route('category.edit', ['id' => $hour['category_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id of day
     * @return \Illuminate\Http\Response
     */
    public function edit($day)
    {
        if(request()->get('category_id')){
            $category_id = request()->get('category_id');
            $selected_day = $day;
            $new_hours = null;
        }else{
            return redirect()->back();
        }

        $hours = $this->model
            ->where('day', $day)
            ->where('category_id', $category_id)
            ->orderBy('hour')
            ->get()
            ->toArray();

        if(request()->get('action') != null && request()->get('action') == 'add')
        {
            $selected_day = null;
        }

        if(request()->get('hours'))
        {
            $new_hours = request()->get('hours');
        }


        return view('admin.hour.edit', [
            'selected_day' => $selected_day,
            'hours' => $hours,
            'category_id' => $category_id,
            'day' => $day,
            'days' => $this->model->getDays(),
            'new_hours' => $new_hours
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $category_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category_id)
    {

        $data = $request->all();
        $this->category_id = $category_id;
        $this->destroy($request->day, false);

        if(isset($data['delete'])){
            $hours = array_diff_key($data['data'], $data['delete']);
        }else{
            $hours = $data['data'];
        }
        foreach ($hours as $hour)
        {
            $this->model::firstOrCreate([
                'category_id' => $hour['category_id'],
                'day' => $hour['day'],
                'hour' => $hour['hour'],
            ],$hour);
        }

        return redirect()->route('category.edit', ['id' => $category_id]);
    }

    public function duplicate($day, $category_id)
    {
        return view('admin.hour.duplicate', [
            'category_id' => $category_id,
            'days' => $this->model->getDays($category_id),
            'day' => $day,
        ]);

    }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $category_id
         * @return \Illuminate\Http\Response
         */
    public function copy(Request $request, $category_id)
    {

        $hours = $this->model
            ->where('day', $request->clone_day)
            ->where('category_id', $category_id)
            ->orderBy('hour')
            ->get()
            ->toArray();

        foreach ($hours as $hour)
        {
            $hour['day'] = $request->day;

            $this->model::firstOrCreate([
                'category_id' => $hour['category_id'],
                'day' => $hour['day'],
                'hour' => $hour['hour'],
            ],$hour);
        }

        return redirect()->route('category.edit', ['id' => $category_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $day
     * @param  int  $category_id
     * @param  int  $redirect
     * @return \Illuminate\Http\Response
     */
    public function destroy($day, $redirect = true)
    {

        $this->model::where('category_id', $this->category_id)
            ->where('day', $day)->delete();

        if($redirect){
            return redirect()->route('category.edit', ['id' => $this->category_id]);
        }
    }
}
