<?php

namespace App\Http\Controllers\Admin;

use App\Schedule;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchedulesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = new Schedule();
        $schedule->order_id = $request->id;
        $schedule->date = $request->date;
        $schedule->start = $request->time;
        $schedule->save();

        $schedules = Schedule::where('order_id', $request->id)
        ->orderBy('start', 'asc')
        ->get();

        return response()->json($schedules);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $schedule = Schedule::find($request->id);
        $schedule->start = $request->time;
        $schedule->save();
        return response()->json(
            Schedule::where('order_id', $request->order_id)
            ->orderBy('start', 'asc')
            ->get()
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $schedule = Schedule::find($request->id);
        $schedule->delete();

        return response()->json(
            Schedule::where('order_id', $request->order_id)
            ->orderBy('start', 'asc')
            ->get());
    }
}
