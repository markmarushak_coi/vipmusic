<?php

namespace App\Http\Controllers\Admin;

use App\Discount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountController extends Controller
{

    private $model;

    public function __construct()
    {
        $this->model = new Discount();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(request()->get('category_id')){
            $category_id = request()->get('category_id');
        }else{
            return redirect()->back();
        }

        return view('admin.discount.create', [
            'category_id' => $category_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'only_one_day' => isset($request->only_one_day) ? 1 : 0
        ]);

        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required',
            'value' => 'required',
            'min_hour' => 'required',
            'min_day' => 'required',
            'type' => 'required',
            'only_one_day' => 'integer'
        ]);


        $discount = $this->model::create($request->all());

        if(isset($discount->id)){
            return redirect()->route('category.edit', ['category_id' => $request->category_id]);
        }else{
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.discount.edit', [
            'category_id' => $id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model::destroy($id);
        return redirect()->back();
    }
}
