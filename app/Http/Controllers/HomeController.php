<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Category;
use App\Hour;
use App\Instrument;
use App\Repository\CalendarRepository;
use App\Repository\SchedulesRepository;
use App\Schedule;
use App\Setting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    private $calendar;

    private $schedule;

    public $cart = [];

    public function __construct(CalendarRepository $calendar, SchedulesRepository $schedule)
    {
        $this->calendar = $calendar;

        $this->schedule = $schedule;

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // get client cart or return empty array
        if(!empty( Cart::getData() ))
        {
            $this->cart = Cart::$cart;
            // recover lost schedules
            // from time to time we can lose data of schedules
            // but by order_id we can restore them
            $this->cart['schedules'] = $this->calendar->getSchedulesByOrder($this->cart['id'], $this->cart['order']['category_id']);

        }



        // load category
        $categories = Category::with(['children', 'discounts', 'reservations'])
            ->where('parent_id', 0)
            ->get();

        // load relations with children category
        foreach ($categories as $category) {
            $category->reservations;
            foreach ($category->children as &$child) {
                $child->children;
                $child->discounts;
                $child->reservations;
                $child->days = Hour::byDaysWithHour($child->id);
            };

            $category->days = Hour::byDaysWithHour($category->id);
        }


        // load instruments
        $instruments = Instrument::all()->toJson();

        // load all schedules by month
        $schedules = $this->schedule->getSchedules(date('Y-m-d', time()), date('Y-m-31', time()));

        // send to user him session id
        $session_id  = Session::getId();

        return view('home', [
            'categories' => $categories,
            'instruments' => $instruments,
            'cart' => $this->cart,
            'schedules' => $schedules,
            'session_id' => $session_id
        ]);
    }

    public function about()
    {

        $settings = Setting::all()->toJson();
        $instruments = Instrument::all()->toJson();

        return view('about', [
            'settings' => $settings,
            'instruments' => $instruments
        ]);
    }
}
