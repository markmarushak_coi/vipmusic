<?php

namespace App\Http\Controllers\API\v1;

use App\Cart;
use App\Company;
use App\Repository\CalendarRepository;
use App\Repository\SchedulesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{

    public $cart = [];

    public $calendar;

    public $schedule;

    public function __construct(CalendarRepository $calendar, SchedulesRepository $schedule)
    {
        $this->calendar = $calendar;

        $this->schedule = $schedule;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cart()
    {
        // get client cart or return empty array
        if (!empty(Cart::getData())) {
            $this->cart = Cart::$cart;
            // recover lost schedules
            // from time to time we can lose data of schedules
            // but by order_id we can restore them
            $this->cart['schedules'] = $this->calendar->getSchedulesByOrder($this->cart['id'], $this->cart['order']['category_id']);

        }

        return response(
            [
                'message' => 'Корзина загружена',
                'data' => $this->cart
            ],
            200
        );
    }

}