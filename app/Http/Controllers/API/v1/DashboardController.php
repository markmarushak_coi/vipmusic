<?php

namespace App\Http\Controllers\API\v1;

use App\Cart;
use App\Events\DashboardSchedulesEvent;
use App\Http\Controllers\Controller;
use App\Jobs\SaveOrderJob;
use App\Order;
use App\Repository\MainRepository;
use App\Repository\OrderRepository;
use App\Repository\SchedulesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DashboardController extends Controller
{

    /**
     * The array return total() function
     * @var array
     */
    private $totals;

    /**
     * List messages
     * @var
     */
    private static $messages = [];

    /**
     * @var MainRepository
     */
    public $repository;

    /**
     * @var
     */
    private $orderRepository;

    private $schedules;

    /**
     * In constructor use repository 
     * 
     * DashboardController constructor.
     * @param MainRepository $repository
     * @param OrderRepository $orderRepository
     */
    public function __construct(MainRepository $repository,
                                SchedulesRepository $schedules,
                                OrderRepository $orders)
    {
        $this->repository = $repository;
        $this->orderRepository = $orders;
        $this->schedules = $schedules;
    }

    public function addSchedules(Request $request)
    {


        // user send in system that chosen schedules
        // system send all user block to the time
        DashboardSchedulesEvent::dispatch($request->selectMonth, $this->schedules->toCalendarSchedule($request->schedules));


        // system calc total and sale order of user
        $this->totals = $this->orderRepository->total($request->all(), $request->order['category_id']);



        // update total and sale in $request
        $request->merge($this->totals);



        // create order
        $_order = $request->order;



        // add string 'empty' in name which have not value
        foreach (Arr::except($_order, ['hash','category_id', 'callback']) as $name => $item){
            if(empty($item) || $item === false || $item === true){
                $_order[$name] = 'empty';
            }else{
                $_order[$name] = $item;
            }
        }



        // check callback signal
        $_order['callback'] = $_order['callback'] == 'on' ? 1 : 0;




        // generate hash order for interkassa
        if(empty($order['hash']) || $_order['hash'] == 'empty'){
            $_order['hash'] = 'VIP_' . rand(1000011, 99999999999);
        }



        // check order need update or create new
        if(empty($order = Order::find($request->id))){
            $_order['timeout'] = 0;
            $order = $this->repository->order->create($_order);
        }else{
            $update = $this->repository->order->update($order, $_order);
        }



        // send to user fail
        if(!$order || !isset($order->id) || isset($update) && !$update){
            return false;
        }



        // update order in request
        $request->merge([
            'order' => $order,
            'id' => $order->id
        ]);



        // update client cart
        Cart::updateCart($request->all());



        // save order in db and sending to all users information about schedules by month
        SaveOrderJob::dispatch($order, $request, $this->repository);



        // send client messages about errors
        // send totals with amount and sale
        // send id and hash order for sync order
        return response()->json([
            'messages' => self::getMessages(),
            'totals' => $this->totals,
            'id' => $order->id,
            'hash' => $order->hash
        ]);
    }

    /**
     * The function add messages to array
     * @param $message
     */
    public static function setMessage($message){
        array_push(self::$messages, $message);
    }

    /**
     * The function return list messages
     * @return mixed
     */
    public static function getMessages()
    {
        return self::$messages;
    }


}