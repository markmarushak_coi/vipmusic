<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Repository\OrderRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $repository;

    /**
     * OrderController constructor.
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {

    }

    /**
     * @param Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function total(Request $request)
    {

        $rules = [
            'order.category_id' => 'required|integer',
            'schedules' => 'required|array',
            'order' => 'required'
        ];

        $messages = [
            'required' => ':attribute не передан',
            'array' => ':attribute не в том формате',
            'integer' => ':attribute не целочислеенное'
        ];

        $this->validate($request, $rules, $messages);

        $data = $request->all();

        $category_id = $data['order']['category_id'];

        $total = $this->repository->total($data, $category_id);

        return response($total, 200);
    }

}