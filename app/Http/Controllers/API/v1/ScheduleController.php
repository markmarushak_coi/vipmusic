<?php

namespace App\Http\Controllers\API\v1;

use App\Cart;
use App\Events\DashboardEvent;
use App\Events\DashboardSchedulesEvent;
use App\Http\Controllers\Controller;
use App\Order;
use App\Repository\SchedulesRepository;
use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{

    private $schedule;

    public function __construct(SchedulesRepository $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id)
    {

        if(!empty(request()->get('date'))){
            if($this->schedule->deleteByOrderAndDate($order_id, request()->get('date'))){
                return response([
                    'status' => 'success',
                    'message' => 'Записи очищены'
                ], 200);
            }
        }else if($this->schedule->deleteByOrder($order_id)){

            return response([
                'status' => 'success',
                'message' => 'Записи очищены'
            ], 200);
        }

        if(Cart::getData()){
            DashboardEvent::dispatch(
                Cart::getMonth(),
                $this->schedule->getSchedules(Cart::getPrev(), Cart::getNext())
            );
        }

        return response([
            'status' => 'failed',
            'message' => 'Записи не очищены'
        ], 200);

    }

    public function between($from, $to)
    {

        $schedules = $this->schedule->getSchedules($from, $to);

        if (count($schedules)) {
            $result = [
                'status' => 'success',
                'message' => 'Список записей на этот месяц',
                'schedules' => $schedules
            ];
        }else{
            $result = [
                'status' => 'failed',
                'message' => 'Список записей пуст'
            ];
        }

        return response($result,200);
    }

}
