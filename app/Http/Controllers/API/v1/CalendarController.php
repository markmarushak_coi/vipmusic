<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Order;
use App\Repository\CalendarRepository;

class CalendarController extends Controller
{

    private $calendar;

    protected $days;

    protected $schedules;

    protected $selected = [];

    public function __construct(CalendarRepository $calendar)
    {
        $this->calendar = $calendar;
    }

    public function getDays()
    {
        return $this->days;
    }

    public function getSchedules($from, $to)
    {
        return $this->calendar->getSchedules($from, $to);
    }

    public function getSelected()
    {
        return $this->selected;
    }

}