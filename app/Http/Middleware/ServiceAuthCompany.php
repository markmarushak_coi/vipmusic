<?php

namespace App\Http\Middleware;

use App\Company;
use Closure;
use Illuminate\Http\Request;

class ServiceAuthCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->bearerToken()){

            $company = Company::where('api_key', $request->bearerToken())
                ->first();

            /**
             * if isset company in the database
             */
            if(!isset($company->id)){
                return response('Company not found', 200);
            }


            /**
             * basic validation for api
             */
            if($this->validate($company, $request)){
                return $next($request);
            }else{
                return response('Company is suspend', 403);
            }
        }


    }

    /**
     * Check company if company have status paid then this function pass
     *
     * @param Company $company
     * @param $request
     * @return bool
     */
    private function validate(Company $company, Request $request){

        if(!$company->isActive()){
            return false;
        }

        if(!$company->isValidApi($company->email, $request->bearerToken())){
            return false;
        }

        return true;
    }
}
