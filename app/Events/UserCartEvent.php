<?php

namespace App\Events;

use App\Jobs\PendingOrderJob;
use App\Order;
use App\Repository\MainRepository;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Session;

class UserCartEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $user_id;

    public $order;

    public $schedules = [];


    /**
     * Create a new event instance.
     * @param $user_id
     * @param $order
     * @param $jobRepository
     * @return void
     */
    public function __construct($user_id, Order $order, MainRepository $jobRepository)
    {
        $this->user_id = $user_id;

        $this->order = $order;

        $this->schedules = $jobRepository->schedules->getSchedulesByOrder($order->id, $order->category_id);



    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user.$this->user_id");
    }

}
