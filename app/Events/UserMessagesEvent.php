<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserMessagesEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $messages = [];

    public $user_id;

    public $crush = [];

    /**
     * Create a new event instance.
     * @param $user_id
     * @param $messages array that contains error messages
     * @param $crush array contains the time that already booked and must be removed from the cart user
     * @return void
     */
    public function __construct($user_id, array $messages = [], array $crush = [])
    {
        $this->user_id = $user_id;
        $this->messages = $messages;
        $this->crush = $crush;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user.$this->user_id");
    }
}
