<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'order_id',
        'total',
        'discount',
        'status'
    ];

    public function orders()
    {
        return $this->hasOne(Order::class, 'mo', 'order_id');
    }

    public function validate(array $data)
    {
        $v = Validator::make($data, [
            'order_id' => [
                'required'
            ],
            'total' => 'required',
            'discount' => 'required',
            'status' => 'required'
        ]);

        if ($v->fails()) {
            return false;
        } else {
            return true;
        }

    }
}
