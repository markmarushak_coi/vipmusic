<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    const STATUSES = [
        'paid' => true,
        'pending' => false,
        'suspend' => false,
        'unverified' => false
    ];

    const INTERATION_HASH = 478;

    const API_L = 20;
    const SECRET_L = 35;

    protected $fillable = [
        'id',
        'secret_key',
        'api_key',
        'status', // status paid, pending, suspend
        'name',
        'verification',
        'payment',
    ];

    /**
     * The function is responsible for status company
     * @return bool
     */
    public function isActive()
    {

        if (self::STATUSES[$this->status]) {
            return true;
        }

        return false;
    }

    public function isValidApi($salt, $api_key)
    {
        $secret_key = hash_pbkdf2("sha256", $salt, $api_key, self::INTERATION_HASH, 35);

        if($secret_key === $this->secret_key){
            return true;
        }

        return false;
    }

    /**
     * The function return new api key
     * @param $salt
     * @return mixed
     */
    public function apiKey($salt){
        return hash_pbkdf2("sha256", $salt, false, self::INTERATION_HASH, 20);
    }

    public function secretKey($text, $salt){
        return hash_pbkdf2("sha256", $text, $salt, self::INTERATION_HASH, 35);
    }
}
