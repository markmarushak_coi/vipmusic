<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Cart extends Model
{

    public static $cart;

    public $fillable = [
        'session',
        'order_id',
        'timeout',
        'data_object'
    ];

    public static function serialize($data)
    {
        return serialize($data);
    }

    public static function unserialize($data)
    {
        return unserialize($data);
    }

    /**
     * @param $cart
     */
    public static function updateCart($cart)
    {

        $session_id = Crypt::decryptString(Cookie::get(Session::getName()));

        if (isset($session_id)) {
            Session::put('cart', $cart);
            $update['session'] = $session_id;
        } else {
            report(new \Exception("Cart can't update session not found"));
        }

        if (isset($cart['id'])) {
            $update['order_id'] = $cart['id'];
        } else {
            report(new \Exception("Cart can't update order ID not found"));
        }

        // todo: timeout for cart need add to job for clear with order
        $update['timeout'] = now();

        $update['data_object'] = self::serialize($cart);

        Cart::updateOrCreate([
            'session' => $session_id,
            'order_id' => $cart['id']
        ], $update);

    }

    /**
     * The function get cart data from table
     *
     * @return mixed|null
     */
    public static function getData()
    {
        Session::put('auth_id', Session::getId());

        $cart = self::where('session', Session::getId())->first();

        // client may open web site through week and system forgot session id
        // we must try restore session id by ip_address
        if (!isset($cart)) {
            $session = DB::table('sessions')
                ->where('ip_address', request()->ip())
                ->orderByDesc('last_activity')
                ->first();

            if (isset($session->id)) {
                $cart = self::where('session', $session->id)->first();

                // reopen last session from this ip
                Session::setId($session->id);
            }
        }

        if ($cart) {
            return Cart::$cart = self::unserialize($cart->data_object);
        }

        return null;
    }

    public static function getMonth()
    {
        if (!empty(Cart::$cart['selectMonth'])) {
            return Cart::$cart['selectMonth'];
        }

        return null;
    }

    public static function getPrev()
    {
        if (!empty(self::getMonth())) {
            return date('Y-m-01', strtotime(self::getMonth()));
        }

        return null;
    }

    public static function getNext()
    {
        if (!empty(self::getMonth())) {
            return date('Y-m-31', strtotime(self::getMonth()));
        }

        return null;
    }

}
