<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderInstrument extends Model
{
    public function order()
    {
        return $this->hasOne( Order::class, 'order_id', 'id');
    }

    public function instrument()
    {
        return $this->belongsTo( Instrument::class, 'instrument_id', 'id');
    }

    public $timestamps = false;
}
