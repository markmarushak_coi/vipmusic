<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'value',
        'type',
        'min_hour',
        'min_day',
        'min_day',
        'one_day',
        'only_one_day'
    ];

    public function getEnding()
    {
        return $this->type == 'percent' ? '%' : config('app.currencies')[config('app.default_currency')];
    }

    public function getValue()
    {
        return $this->value . ' ' . $this->getEnding();
    }

    public static function calc($category_id, $data)
    {
        $class = new Discount();

        $totals = [
            'main' => 0.00,
            'sale' => 0.00
        ];

        $discounts = $class::where('category_id', $category_id)->orderBy('value', 'desc')->get();

        foreach ($data['schedules'] as $day) {
            /**
             * while on the main price of hour and add to total
             */
            foreach ($day['hours'] as &$time) {

                $h = Hour::where('day', $day['day'])
                    ->where('category_id', $day['category_id'])
                    ->where('hour', $time)
                    ->first();

                // add to main price
                $totals['main'] += $h->price;

                // clone object Hour
                $time = $h;
            }

            /**
             * while on discounts from larger to smaller
             */

            $sales = [];
            foreach ($discounts as $discount) {

                if ($class->validateDiscount($day, $discount)) // if found discount add to total and break
                {
                    $sales[] = $class->validateDiscount($day, $discount);
                }

            }

            if (count($sales)) {
                $totals['sale'] += min($sales);
            }
        }

        return $totals;
    }

    public static function findSale($category_id)
    {
        return self::where('category_id', $category_id)->get()->count();
    }

    private function validateDiscount($day, $discount)
    {
        $error = [];

        $min_hour = count($day['hours']);

        if ($discount->min_hour > $min_hour) {
            $error[] = 'min_hour does not match';
        }

        if ($discount->only_one_day) {
            if ($discount->one_day != $day['date']) {
                $error[] = 'one_day does not match';
            }
        }

        $total = 0.00;
        $sale = 0.00;

        foreach ($day['hours'] as $key => $day) {
            if ($discount->min_hour - 1 >= $key) {
                $sale += $day->price;
            } else {
                $total += $day->price;
            }
        }

        if ($discount->type == 'percent') {
            $sale -= ($sale * $discount->value / 100);
        } else if ($discount->type == 'static') {
            $sale = $discount->value;
        }

        $total += $sale;

        if (count($error)) {
            return false;
        }

        return $total;

    }
}
