<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class Order extends Model
{
    protected $table = 'orders';

    public $incrementing = true;

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'hash',
        'band',
        'phone',
        'email',
        'category_id',
        'callback',
        'timeout'
    ];

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function instruments()
    {
        return $this->hasMany(OrderInstrument::class);
    }

    public function payment()
    {
        return $this->hasOne(Invoice::class);
    }

    /*
     * For deleted
     *
     */
    public function payments()
    {
        return $this->hasMany(Invoice::class);
    }

    public function validate(array $data)
    {

        $v = Validator::make($data, [
            'hash' => 'required',
            'name' => 'required',
            'band' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'category_id' => 'required',
            'callback' => 'required'
        ]);

        if ($v->fails()) {
            return false;
        } else {
            return true;
        }

    }

}
