<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'value',
        'type',
        'min_hour',
        'min_day',
        'min_day',
        'min_week',
    ];

    public function getEnding()
    {
        return $this->type == 'percent' ? '%' : config('app.currencies')[config('app.default_currency')];
    }

    public function getValue()
    {
        return $this->value . ' ' . $this->getEnding();
    }

    public static function findSale($category_id)
    {
        return self::where('category_id', $category_id)->get()->count();
    }

    public static function calc($category_id, $data)
    {
        $class = new Reservation();

        $weeks = [];

        $data['totals'] = [
            'main'      => 0.00,
            'sale'      => 0.00,
            'min_week'  => 0,
            'min_day'   => 0,
            'min_hour'  => 0
        ];

        foreach ($data['schedules'] as &$day)
        {
            /**
             * while on the main price of hour and add to total
             */
            foreach ($day['hours'] as &$time){

                $h = Hour::where('day', $day['day'])
                    ->where('category_id', $day['category_id'])
                    ->where('hour', $time)
                    ->first();

                // add to main price
                $data['totals']['main'] += $h->price;

                // clone object Hour
                $time = $h;
                $data['totals']['min_hour']++;

            }

            // push number week of year
            array_push($weeks, date('W', strtotime($day['date'])));
            ++$data['totals']['min_day'];

        }

        // filter unique weeks
        $data['totals']['min_week'] = count(array_unique($weeks));

        $reservations = $class::where('category_id', $category_id)->orderBy('value', 'desc')->get();

        /**
         * while on discounts from larger to smaller
         */
        foreach ($reservations as $reservation){

            if($class->validateReservation($data, $reservation)) // if found discount add to total and break
            {
                $data['totals']['sale'] += $class->validateReservation($data, $reservation);
                break;
            }

        }

        return $data['totals'];
    }

    private function validateReservation($data, $discount)
    {
        $error = [];

        if ($discount->min_hour > $data['totals']['min_hour']){
            $error[] = 'min_hour does not match';
        }

        if ($discount->min_day > $data['totals']['min_day']){
            $error[] = 'min_day does not match';
        }

        if ($discount->min_week > $data['totals']['min_week']){
            $error[] = 'min_week does not match';
        }

        if(count($error)){
            return false;
        }

        $total = $data['totals']['main'];

        if($discount->type == 'percent'){
            $total -= ($total * $discount->value / 100);
        }else if ($discount->type == 'static'){
            $total = $discount->value;
        }

        return $total;

    }
}
