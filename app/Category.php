<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $fillable = [
        'parent_id',
        'name',
        'sub_name',
        'min_hour'
    ];

    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function hours()
    {
        return $this->hasMany(Hour::class, 'category_id', 'id');
    }

    public function days()
    {
        return Hour::byDaysWithHour($this->id);
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class, 'category_id', 'id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'category_id', 'id');
    }

}