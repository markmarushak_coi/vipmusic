<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'mark',
            'email' => 'mark@app.com',
            'type' => 'admin',
            'password' => Hash::make('password'),
        ]);
    }
}
