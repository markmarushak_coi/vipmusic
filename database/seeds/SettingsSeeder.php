<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $data = [
        'cost' => 100,
        'start_work' => 9,
        'finish_work' => 19,
        'min_hours_month' => 8,
        'sale_month' => 15,
        'min_hours_sale' => 2,

    ];

    public function run()
    {
        foreach ($this->data as $name => $value){
            $setting = new Setting();
            $setting->name = $name;
            $setting->value = $value;
            $setting->type = 'system';
            $setting->save();
        }
    }
}
