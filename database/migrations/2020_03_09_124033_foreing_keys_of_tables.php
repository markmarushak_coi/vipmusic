<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeingKeysOfTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        // order invoices
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
        });

        // order category_id
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
        });

        // order schedules
        Schema::table('schedules', function (Blueprint $table) {
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
        });

        // order instruments
        Schema::table('order_instruments', function (Blueprint $table) {
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');

            $table->foreign('instrument_id')
                ->references('id')
                ->on('instruments')
                ->onDelete('cascade');
        });

        // cart order_id
        Schema::table('carts', function (Blueprint $table) {
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
        });

        // discount category_id
        Schema::table('discounts', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')->onDelete('cascade');
        });

        // reservation category_id
        Schema::table('reservations', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')->onDelete('cascade');
        });

        Schema::table('hours', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        Schema::table('schedules', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
        });

        Schema::table('order_instruments', function (Blueprint $table) {
            $table->dropForeign(['instrument_id']);
            $table->dropForeign(['order_id']);
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
        });

        Schema::table('discounts', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        Schema::table('reservations', function (Blueprint $table) {
         $table->dropForeign(['category_id']);
        });

        Schema::table('hours', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

    }
}
