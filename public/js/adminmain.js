jQuery(document).ready(function($) {
    /*
         THE CODE SHOULD BE IN START THE FILE
      */
    $.get('/setting?type=json', function (data) {
        for(let i=0; i<data.length;i++){
            settings[data[i].name] = data[i].value
        }
    })

    /*
        THE CODE SHOULD BE IN START THE FILE
     */


	$('.menu li').click(function() {
		$('.menu li').removeClass('active');
		$(this).addClass('active');
	});


	let calendar = $("#myCalendar-1").ionCalendar({
		onReady: false,
		lang: "ru",                     // язык календаря
		sundayFirst: false,             // первый день недели
		years: "2018-2050",                    // диапазон лет
		format: "YYYY.MM.DD",           // формат возвращаемой даты
		role: "admin",           // формат возвращаемой даты
		loop: true,           // формат возвращаемой даты
		onClick: function(date){        // клик по дням вернет сюда дату
			getDaySchedule(date);
			// Обновление инфы о заказе при выборе даты/времени
			$('#confirmBooking').click(function(){
				if ($('#tab tr.select').length==0) {
					alert('Вы не выбрали время репетиции!');
				}
				else if ($('#tab tr.select').length < 2 && $('.menu li.active').hasClass('repetition')) {
					alert('Необходимо выбрать миним 2 часа для репетиции!');
				}
				else{
					// дата
					let dateText=date[0].split(".");
					let dateTitle = dateText[2]+' '+monthToText(parseInt(dateText[1]))+' '+dateText[0];
					$('.main-info table tr:first-child td').eq(1).text(dateTitle);
					$('input[name="date_exec"]').val(dateText[0]+'-'+dateText[1]+'-'+dateText[2]);
					// время (часы)
					let hourArr=[];
					$('#tab tr.select').each(function(){
						hourArr.push($(this).find('input[type="hidden"]').attr('name'));
					});
					$('input[name="hour_start"]').val(hourArr);
					$('input[name="dur"]').val(hourArr.length);

					for(let o in hourArr){
						$('#dur_row1 td').eq(1).text(hourArr[0]+':00');
						$('#dur_row2 td').eq(1).text(parseInt(hourArr[hourArr.length-1])+1+':00');
					}

					// Стоимость заказа
					let price=parseInt(hourArr.length)*100;
					$('#price_row, .total-amount span.amount').text(price+' грн');
					$('input[name="totalOrderPrice"]').val(price);

					$('input[name="type"]').val($('.main li.active').attr('class').replace('active','').trim())

					hideBg($('.tab-bg'));

				}
			});

			$('#resetBooking').click(function(){
				$('#tab tr').each(function(){
					if ($(this).hasClass('select')) {
						$(this).find('input[type="hidden"]').val(0);
						$(this).toggleClass('select');
					}
					else{
						$(this).show();
					}
				});
			});
		}
	});

	setInterval(function () {
		$.get('/admin/update_orders', function (data) {
			orders_data = data
		})

	}, 3600)



	$('.ic__header').append('<div class="ic__title"><span class="month"></span><span class="year"></span></div>');

	$('.back, .next').click(function(event){
		event.preventDefault();
		var year = $('.ic__year select').text();
		var month = $('.ic__month option:selected').text();
		$('.ic__header:before').css({
			'content': year+ ' '+ month
		})
	});

	function monthToText(month=1){
		let textMonths=['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
		return textMonths[month-1];
	}

	function getDaySchedule(date_in){
		var takenHours;
		dateText = date_in[0].split(".");
		dateText = dateText[0]+'-'+dateText[1]+'-'+dateText[2]
		// $.ajax({
		// 	type:"GET",
		// 	url:"/adminordersbydate?date="+dateText,
		// 	success: function(res) {
		// 		takenHours=res;
		// 		$('.ic__day .tab').remove();
		// 		let dateText=date_in[0].split(".");
		// 		let title = dateText[2]+' '+monthToText(parseInt(dateText[1]))+' '+dateText[0];
		// 		var el = date_in[1];
		// 		var pos = date_in[2];
		// 		var table = getTable(title, pos, takenHours);
		// 		$('#tab .unavailable').parent().attr('colspan','2');
		// 	},
		// });

        $.ajax({
            type:"GET",
            url:"/admin/order/show",
            data: {date: dateText},
            dataType:"html",
            success: function(res) {
                $('#info').empty()
                $('#info').append(res)

                $('.orders').click(function () {
                    let parent = $(this).attr('id')
                    $('.schedules:not('+"."+parent+')').removeClass('show');
                    $('.schedules.'+parent).toggleClass('show');
                })
            },
        });

	}

	function getTable(d, pos, taken){
		var data=[];

		for (var i = 9; i < 19; i++) {
			if (typeof taken[i] != 'undefined'){
				if (taken[i].status.indexOf('success') != -1) {
					data.push({
						'id': i,
						'order_id': taken[i].order_id,
						'time': i+':00 -'+(i+1)+':00',
						'status': 'занято',
                        'name': taken[i].name,
                        'type': taken[i].type,
					});
				}
				else if (taken[i].status.indexOf('pending') != -1){
					data.push({
                        'id': i,
                        'order_id': taken[i].order_id,
						'time': i+':00 -'+(i+1)+':00',
						'status': 'ожидаеться оплата',
                        'name': taken[i].name,
                        'type': taken[i].type,
					});
				}
			}
			else{
				data.push({
					'id': i,
                    'order_id': '',
                    'time': i+':00 -'+(i+1)+':00',
					'status': 'доступно',
                    'name': '',
                    'type': '',
                    'instruments': '',
				});
			}

		}


		if ($('#solo').is(':checked')) {
			// = 'недоступно';
			for (var i=7; i < data.length; i++){
				data[i].status = 'недоступно';
			}
		}

		$('#tab').DataTable({
			paging: false,
			ordering: false,
			responsive: false,
			searching:false,
			destroy: true,
			autoWidth: true,
			info: false,
			order: [[1, 'desc']],
			data: data,

			rowId: "id",

			columnDefs: [
				{
					targets: 0,
					data: {time:'time',status: 'status'},
					render: function (data, type, full, meta) {
                        let result
					    switch (data.status) {
                            case "недоступно":
                                result = `<span class="unavailable-time time text-red">`+data.time+`</span>`;
                                break;
                            case "занято":
                                result = `<span class="unavailable-time time text-success">`+data.time+`</span>`;
                                break;
                            case "ожидаеться оплата":
                                result =  `<span class="unavailable-time time text-info">`+data.time+`</span>`;
                                break;
                            default:
                                result =  `<span class=" time main-color">`+data.time+`</span>`;
                                break;
                        }
                        return result
					},
				},
				{
					targets: 1,
					data: 'status',
					render: function (data, type, full, meta) {
						return data
					},
				},
                {
                    targets: 2,
                    data: 'type',
                    render: function (data, type, full, meta) {
                        switch (data) {
                            case 'repetition':
                                return 'рпетиция'
                            case 'record-music':
                                return 'запись'
                            case 'lessons':
                                return 'урок'
                            default:
                                return ''

                        }
                    },
                },
                {
                    targets: 3,
                    data: 'name',
                    render: function (data, type, full, meta) {
                        return data
                    },
                },
				{
					targets: 4,
					data: {buy: 'buy' ,status:'status', id: 'id', order_id: 'order_id'},
					render: function (data, type, full, meta) {
					    if (data.order_id != ''){
                            return `<a href="/`+data.order_id+`">подробнее</a>`
                        }else {
					        return ''
                        }
					},
				}
			]
		});
		var table = $('#component');

		table.find('header').text(d);

		// Выбор дня и времени
		$('#tab td label').click(function(){
			$(this).parent().parent().toggleClass('select');
			let selectedHour = $(this).parent().parent().hasClass('select');
			if (selectedHour) {
				$(this).find('input[type="hidden"]').val(1);
				if ($('#tab tr.select').length==1) {
					$('#tab tr').each(function(){
						if ($(this).hasClass('select')) {
							return false;
						}
						else{
							// $(this).hide();
						}
					});
				}
			}
			else{
				$(this).find('input[type="hidden"]').val(0);
			}

			var text = $(this).prev().text();
			if(text == 'выбрать'){
				$(this).prev().text('выбрано');
			}else{
				$(this).prev().text('выбрать');
			}
			let idx = $(this).index
		});


		if ($(document).width() > 768) {
			table.css(pos);
		}else {
			table.css($('.ic__days').offset());
		}

		$('#component').removeClass('hide');

		if(!$('*').is('.tab-bg')){
			$('body').append('<div class="tab-bg" onClick="hideBg($(this));"></div>');
		}


		// var table = $('#component');
		// table.find('header').text(d);
		// return table[0];
	}

});



function hideBg(el){
	$('#component').addClass('hide');
	$(el).remove();
}