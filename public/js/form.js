document.addEventListener("DOMContentLoaded", function(event) {

    jQuery('#validatorTrigger').click(function(e){
        e.preventDefault();
        validateOrderForm();
    })
    function validateOrderForm(){
        let instr_arr=[];
        let errors = []

        /**
         * Instruments
         */
        jQuery('.mus-instr input:checked').each(function(){
            instr_arr.push(this.value);
        });

        /**
         * User data name, email etc.
         * */
        jQuery('.f-block.inp-text input[required]').each(function(){

            if(this.value !== ''){
                order[this.getAttribute('name')] = this.value
                this.classList.remove('error')
                delete errors['user_data']
            }else{
                errors['user_data'] = 'error'
                if(!this.classList.contains('error'))
                {
                    this.classList.add('error')
                }
            }
        })

        if( order.date &&
            order.amount&&
            order.type) {
            delete errors['date']
        }else{
            errors['date'] = 'error'
        }

        order.instruments = instr_arr
        order.callback = jQuery('#call-back')[0].checked
        order.month = jQuery('#buy-month')[0].checked

        if(Object.keys(errors).length <= 0){
            $.ajax({
                method: 'POST',
                url: 'createOrder',
                data: {_token: jQuery("*[name='csrf-token']").attr('content'), data:order},
            }).done(function (data) {
                let form = $('div')
                form.html(data.form)
                $('#interkassa').submit()
            })
        }
    }
});

