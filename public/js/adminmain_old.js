jQuery(document).ready(function($) {




	$('.menu li').click(function() {
		$('.menu li').removeClass('active');
		$(this).addClass('active');
	});

	

	$("#myCalendar-1").ionCalendar({
		onReady: false,
	    lang: "ru",                     // язык календаря
	    sundayFirst: false,             // первый день недели
	    years: "2018-2050",                    // диапазон лет
	    format: "YYYY.MM.DD",           // формат возвращаемой даты
	    onClick: function(date){        // клик по дням вернет сюда дату
	    	getDaySchedule(date);
	    	
	    }  
	});



	$('.ic__header').append('<div class="ic__title"><span class="month"></span><span class="year"></span></div>');

	$('.back, .next').click(function(event){
		event.preventDefault();
		var year = $('.ic__year select').text();
		var month = $('.ic__month option:selected').text();
		$('.ic__header:before').css({
			'content': year+ ' '+ month
		})
	});

	function monthToText(month=1){
		let textMonths=['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
		return textMonths[month-1];
	}

	function getDaySchedule(date_in){
		var takenHours;
		$.ajax({
		    type:"GET",
		    url:"/adminordersbydate?date="+date_in[0],
		    success: function(res) {
		    	takenHours=res[1];
		    	$('.ic__day .tab').remove();
		    	let dateText=date_in[0].split(".");
		    	let title = dateText[2]+' '+monthToText(parseInt(dateText[1]))+' '+dateText[0];
		    	var el = date_in[1];
		    	var pos = date_in[2];
		    	var table = getTable(title, pos, takenHours);
		    	// Выбор дня и времени
		    	$('#tab td label').click(function(){
		    		let idx = $(this).parent().parent().prop('id');
		    		// Проверяем или есть такой заказ
		    		let stts=0;
		    		res[0].forEach(function(el){
		    			if(el.hour_start<=idx && idx<el.hour_start+el.exec_len){
		    				$('.main-info table').show();
		    				$('.btn-grp #deleteOrderNow').show();
		    				$('.total-amount').show();
		    				$('#createFakeOrderForm').hide();
		    				// дата 
		    				let dateText=date_in[0].split(".");
		    				let dateTitle = dateText[2]+' '+monthToText(parseInt(dateText[1]))+' '+dateText[0];
		    				$('.main-info table tr:first-child td').eq(1).text(dateTitle);
		    				// время (часы)
		    				$('#dur_row1 td').eq(1).text(el.hour_start+':00');
		    				$('#dur_row2 td').eq(1).text((el.hour_start+el.exec_len)+':00');

		    				// Стоимость заказа
		    				$('#price_row td:last-child').text(el.amount_total+' грн');
		    				$('.total-amount span.amount').text(el.amount_paid+' грн');

		    				// Данные заказа
		    				$('#grp_row td:last-child').text(el.name+'; '+el.groupname);
		    				$('#email_row td:last-child').text(el.email);
		    				$('#tel_row td:last-child').text(el.tel);
		    				$('#inst_row td:last-child').text(el.instruments);
		    				$('#orderid_row td:last-child').text(el.id);
		    				stts=1;


		    				hideBg($('.tab-bg'));
		    			}
		    		});
		    		// если время доступно,
		    		if (stts===0){
		    			$('.main-info table').hide();
		    			$('.btn-grp #deleteOrderNow').hide();
		    			$('.total-amount').hide();
		    			$('#createFakeOrderForm').show();
		    			$('input[name="hr_start"]').val(idx);
		    			$('input[name="len"]').val(1);
		    		}
		    	});

		    },
		});

	}

	function getTable(d, pos, taken){
		var data=[];
		for (var i = 9; i < 19; i++) {
			if (taken.includes(i)) {
				data.push({
					'id': i,
					'time': i+':00 -'+(i+1)+':00',
					'price': '100',
					'status': 'занято'
				});
			}
			else{
				data.push({
					'id': i,
					'time': i+':00 -'+(i+1)+':00',
					'price': '100',
					'status': 'доступно'
				});
			}
			
		}

		// test data
		// data = [
		// {
		// 	'id': 9,
		// 	'time': '9:00 - 10:00',
		// 	'price': '100',
		// 	'status': 'доступно'

		// },
		// {
		// 	'id': 10,
		// 	'time': '10:00 - 11:00',
		// 	'price': '100',
		// 	'status': 'доступно'
			
		// },
		// {
		// 	'id': 11,
		// 	'time': '11:00 - 12:00',
		// 	'price': '100',
		// 	'status': 'недоступно'
			
		// },
		// {
		// 	'id': 12,
		// 	'time': '12:00 - 13:00',
		// 	'price': '100',
		// 	'status': 'доступно'
		// },
		// {
		// 	'id': 13,
		// 	'time': '13:00 - 14:00',
		// 	'status': 'занято'
		// },
		// {
		// 	'id': 14,
		// 	'time': '14:00 - 15:00',
		// 	'price': '100',
		// 	'status': 'доступно'
		// },
		// {
		// 	'id': 15,
		// 	'time': '15:00 - 16:00',
		// 	'price': '100',
		// 	'status': 'доступно'
		// },
		// {
		// 	'id': 16,
		// 	'time': '16:00 - 17:00',
		// 	'price': '100',
		// 	'status': 'доступно'
		// },
		// {
		// 	'id': 17,
		// 	'time': '17:00 - 18:00',
		// 	'price': '100',
		// 	'status': 'доступно'
		// },
		// {
		// 	'id': 18,
		// 	'time': '18:00 - 19:00',
		// 	'price': '100',
		// 	'status': 'доступно'
		// },
		
		// ];

		if ($('#solo').is(':checked')) {
			 // = 'недоступно';
			 for (var i=7; i < data.length; i++){
			 	data[i].status = 'недоступно';
			 }
		}

		$('#tab').DataTable({
			paging: false,
			ordering: false,
			responsive: false,
			searching:false,
			destroy: true,
			autoWidth: false,
			info: false,
			order: [[1, 'desc']],
			data: data,

			rowId: "id",

			columnDefs: [
			{
				targets: 0,
				data: {time:'time',status: 'status'},
				render: function (data, type, full, meta) {
					if (data.status == 'недоступно') {
						return `<span class="unavailable-time time ">`+data.time+`</span>`;
					}else if(data.status == 'занято'){
						return `<span class="unavailable-time time red-color">`+data.time+`</span>`;
					}
					else {
						return `<span class=" time main-color">`+data.time+`</span>`;
					}
				},
			},
			{
				targets: 1,
				data: {price:'price',status:'status'},
				render: function (data, type, full, meta) {
					if (data.status == 'недоступно') {
						return `<span class="unavailable">`+ data.status+`</span>`;
					}else if(data.status == 'занято') {
						return `<span class="unavailable red-color">`+ data.status+`</span>`;
					}else {
						return `<span class="price main-color">`+ data.price+` &#8372;</span>`;
					}
				},
			},
			{
				targets: 2,
				data: 'status',
				render: function (data, type, full, meta) {
					if (data == 'недоступно' || data == 'занято') {
						data = "";
					}
					return data
				},
			},
			{
				targets: 3,
				data: {buy: 'buy' ,status:'status', id: 'id'},
				render: function (data, type, full, meta) {
						return `<label href="#" ><span class="main-color text-input">выбрать</span> <input type="hidden" name="`+data.id+`" value="0"></label>`
				},
			}
			]
		});
		var table = $('#component');

		table.find('header').text(d);

		

		if ($(document).width() > 768) {
				table.css(pos);
			}else {
				table.css($('.ic__days').offset());
			}

			$('#component').removeClass('hide');

			if(!$('*').is('.tab-bg')){
				$('body').append('<div class="tab-bg" onClick="hideBg($(this));"></div>');
			}


		// var table = $('#component');
		// table.find('header').text(d);
		// return table[0];
	}
	

});



function hideBg(el){
	$('#component').addClass('hide');
	$(el).remove();
}

