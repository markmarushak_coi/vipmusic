<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>@yield('title', 'VIPMUSIC.STUDIO')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha256-UzFD2WYH2U1dQpKDjjZK72VtPeWP50NoJjd26rnAdUI=" crossorigin="anonymous"/>--}}

    <link rel="stylesheet" href="./public/css/ion.calendar.css">

    <link rel="stylesheet" href="./public/css/main.css">
    <link rel="stylesheet" href="./public/css/app.css?ver={{ env('APP_CSS', '1.0.0.1') }}">

    {{--<script src="public/lib/ion.sound/ion.sound.js"></script>--}}
    <script src="public/js/moment-with-locales.min.js"></script>

    @if(\Request::is(['home', '/']))
        <script>
               let token = '{{ csrf_token() }}'
               let category = null

        </script>
    @endif

</head>
<body>
<div id="app">
    <div id="warning"></div>
    <header class="top">
        <a href="/" class="logo">
            <img src="/public/img/logo.png" alt="logo">
        </a>
    </header>

    @yield('content')

</div>


<script src="/public/js/app.js?ver={{ env('APP_JS', '1.0.0.1') }}"></script>
<script src="{{ asset('js/clear.js') }}"></script>

</body>
</html>