@extends('layout')

@section('title')
VIPMUSIC.STUDIO
@endsection

@section('content')


<style type="text/css">
	h1,h2,h3,h4,h5,p,span, li{
		color: #fff
	}
	a{
		color: #edbe00
	}
	iframe{
		width: 100%;
		max-width: 100vw
	}
	.carousel-item, .carousel-inner, #carouselExampleControls{
    max-width:600px;
    margin:auto
	}
	.carousel-control-next-icon, .carousel-control-prev-icon{
    	width:50px!important;
    	height:50px!important;
	}
</style>

<section class="main">
	<div class="container">
		<div class="row" style="padding-top: 20px;">
			<h2>О нас</h2>
			<p>
				Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.
			</p>
		</div>
		<div class="row">
			<div class="container">
      <h3 style="text-align:center">Видео</h3>
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
          <div class="carousel-item active">
          	<iframe width="560" height="315" src="https://www.youtube.com/embed/vdCEQaI9IRU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="carousel-item">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/sTa6CMHxDU8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="carousel-item">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/ETvUx0fYNK8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
        
        <script>
        $(function() {
  $(".carousel").on("slide.bs.carousel", function(e) {
    var prev = $(this)
      .find(".active")
      .index();
    var next = $(e.relatedTarget).index();
    var video = $("#video-player")[0];
    var videoSlide = $("#video-player")
      .closest(".carousel-item")
      .index();
    if (next === videoSlide) {
      if (video.tagName == "IFRAME") {
        player.playVideo();
      } else {
        createVideo(video);
      }
    } else {
      if (typeof player !== "undefined") {
        player.pauseVideo();
      }
    }
  });
});

function createVideo(video) {
  var youtubeScriptId = "youtube-api";
  var youtubeScript = document.getElementById(youtubeScriptId);
  var videoId = video.getAttribute("data-video-id");

  if (youtubeScript === null) {
    var tag = document.createElement("script");
    var firstScript = document.getElementsByTagName("script")[0];

    tag.src = "https://www.youtube.com/iframe_api";
    tag.id = youtubeScriptId;
    firstScript.parentNode.insertBefore(tag, firstScript);
  }

  window.onYouTubeIframeAPIReady = function() {
    window.player = new window.YT.Player(video, {
      videoId: videoId,
      playerVars: {
        autoplay: 1,
        modestbranding: 1,
        rel: 0
      }
    });
  };
}

        </script>
			
		</div>
		<div class="row" style="padding-top: 20px;">
			<h2>Контакты</h2>
			<ul>
				<li>Моб. тел.: <a href="tel:+380977148788">+380 97 714 87 88</a> (Сергей)</li>
				<li>Email: <a href="mailto:vipmusicstudio1@gmail.com">vipmusicstudio1@gmail.com</a></li>
				<li>Адресс: г. Харьков, ул. Шекспира, 26</li>
			</ul>
		</div>
		<div class="row">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d640.7085422933152!2d36.21452842923693!3d50.03320309871377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a14b7abc2d85%3A0x225903a568854d9a!2z0YPQuy4g0KjQtdC60YHQv9C40YDQsCwgMjYsINCl0LDRgNGM0LrQvtCyLCDQpdCw0YDRjNC60L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2MTAwMA!5e0!3m2!1sru!2sua!4v1569887360323!5m2!1sru!2sua" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
	</div>
</section>

@endsection