<form id="interkassa" method="post" target="_blank" action="https://sci.interkassa.com/">
	<input type="hidden" name="ik_co_id" value="5c5c824f3c1eaf80738b4572">
	<input type="hidden" name="ik_pm_no" value="{{ $order_id }}">
	<p><input type="hidden" name="ik_am" value="{{ $amount }}"></p>
	<input type="hidden" name="ik_cur" value="UAH">
	<input type="hidden" name="ik_desc" value="Оплата за оренду музыкальной студии (заказ №{{ $order_id }}); Плательщик: {{ $name }}">
</form>