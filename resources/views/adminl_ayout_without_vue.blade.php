<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>@yield('title', 'VIPMUSIC.STUDIO')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha256-UzFD2WYH2U1dQpKDjjZK72VtPeWP50NoJjd26rnAdUI=" crossorigin="anonymous" />
	<script src="https://kit.fontawesome.com/42b2b4fbc1.js" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha256-YLGeXaapI0/5IgZopewRJcFXomhRMlYYjugPLSyNjTY=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css" integrity="sha256-YY1izqyhIj4W3iyJOaGWOpXDSwrHWFL4Nfk+W0LyCHE=" crossorigin="anonymous" />
	<link rel="stylesheet" href="/public/css/ion.calendar.css">

	<link rel="stylesheet" href="/public/css/main.css">
	<link rel="stylesheet" href="/public/css/app.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script>
		let settings = {}
		let orders_data = []
	</script>

	@yield('global_js')

</head>
<body>

<header class="top">
	<a href="/" class="logo">
		<img src="/public/img/logo.png" alt="logo">
	</a>
	<nav class="navbar navbar-expand-lg navbar-light p-2 ">
		<a class="navbar-brand" href="#">Админ Панель</a>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav d-flex justify-content-end w-100">
				<li class="nav-item">
					<a class="nav-link @if(\Request::is('admin')) active @endif " href="{{ route('admin.index') }}">Доска</a>
				</li>
				<li class="nav-item">
					<a class="nav-link @if(\Request::is('admin/setting') || \Request::is('admin/setting/*')) active @endif " href="{{ route('setting.index') }}">Настройки</a>
				</li>
			</ul>
		</div>
	</nav>
</header>


@yield('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion.calendar/2.0.2/js/moment-with-locales.min.js" integrity="sha256-+Cj7p4c156QUjuzaBQEy8IRJtnxl4Fg/dGapt13rpoY=" crossorigin="anonymous"></script>
<script src="/public/js/ion.calendar.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js" integrity="sha256-t5ZQTZsbQi8NxszC10CseKjJ5QeMw5NINtOXQrESGSU=" crossorigin="anonymous"></script>
<script src="/public/js/adminmain.js"></script>

</body>
</html>