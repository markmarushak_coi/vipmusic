@extends('layout')

@section('title')
Оплата
@endsection

@section('content')
<section class="main" style="min-height: calc(100vh - 70px)">
	
	<p style="color: #fff;text-align: center;margin-top: 3rem">Оплата прошла успешно.<br>С Вами свяжутся в ближайшее время.</p>

	<script>
		setTimeout(() => {window.location.href = '/'}, 3000)
	</script>
</section>
@endsection