<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>@yield('title', 'VIPMUSIC.STUDIO')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"
		  integrity="sha256-UzFD2WYH2U1dQpKDjjZK72VtPeWP50NoJjd26rnAdUI=" crossorigin="anonymous"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"
		  integrity="sha256-YLGeXaapI0/5IgZopewRJcFXomhRMlYYjugPLSyNjTY=" crossorigin="anonymous"/>
	<link rel="stylesheet"
		  href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css"
		  integrity="sha256-YY1izqyhIj4W3iyJOaGWOpXDSwrHWFL4Nfk+W0LyCHE=" crossorigin="anonymous"/>
	<link rel="stylesheet" href="./public/css/ion.calendar.css">

	<link rel="stylesheet" href="./public/css/main.css">
	<link rel="stylesheet" href="./public/css/app.css">



	<script src="https://cdnjs.cloudflare.com/ajax/libs/ion.calendar/2.0.2/js/moment-with-locales.min.js" integrity="sha256-+Cj7p4c156QUjuzaBQEy8IRJtnxl4Fg/dGapt13rpoY=" crossorigin="anonymous"></script>

@if(\Request::is(['admin']))
		<script>
			let website = JSON.parse('<?= $settings?>')
			let instruments = JSON.parse('<?= $instruments?>')
			let token = '{{ csrf_token() }}'
			let settings = {}
			website.filter(setting => {
				settings[setting.name] = setting.value
			})

		</script>
	@endif

	@yield('global_js')

</head> 
<body>

	<div id="app">
		<div id="warning"></div>
		<header class="top">
			<a href="/" class="logo">
				<img src="/public/img/logo.png" alt="logo">
			</a>
			<nav class="navbar navbar-expand-lg navbar-light p-2 ">
				<a class="navbar-brand" href="#">Админ Панель</a>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav d-flex justify-content-end w-100">
						<li class="nav-item">
							<a class="nav-link @if(\Request::is('admin')) active @endif " href="{{ route('admin.index') }}">Доска</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @if(\Request::is('admin/setting') || \Request::is('admin/setting/*')) active @endif " href="{{ route('setting.index') }}">Настройки</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<dashboard :is_admin="true"></dashboard>

		@yield('content')

	</div>




<script src="/public/js/app.js"></script>


{{--<script src="./public/js/main.js"></script>--}}
{{--<script src="./public/js/form.js"></script>--}}


</body>
</html>