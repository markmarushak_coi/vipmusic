@extends('adminlayout')

@section('title')
Admin.STUDIO
@endsection

@section('content')

<section class="main">
	<h1 style="color: #fff;text-align: center;">Админ Панель</h1>
	<div class="container">
		<div class="row">
			
			<div class="col-sm-8 col-xs-12">
				
				<article class="colendar">
					
					<div class="myCalendar" id="myCalendar-1"><div class="ic__container"><div class="ic__header"><div class="ic__prev back"><i class="fas fa-chevron-left"></i>июль 2019</div><div class="ic__next next">2019 сентябрь<i class="fas fa-chevron-right"></i></div><div class="ic__title"><span class="month">август</span><span class="year">2019</span></div><div class="ic__title"><span class="month"></span><span class="year"></span></div></div><table class="ic__week-head"><tbody><tr><td>понедельник</td><td>вторник</td><td>среда</td><td>четверг</td><td>пятница</td><td>суббота</td><td>воскресенье</td></tr></tbody></table><table class="ic__days"><tbody><tr><td class="ic__day-empty"><span class="num">28</span></td><td class="ic__day-empty"><span class="num">29</span></td><td class="ic__day-empty"><span class="num">30</span></td><td class="ic__day-empty"><span class="num">1</span></td><td class="ic__day-empty"><span class="num">2</span></td><td class="ic__day-empty"><span class="num">3</span></td><td class="ic__day-empty"><span class="num">4</span></td></tr><tr><td class="ic__day-empty"><span class="num">5</span></td><td class="ic__day"><span class="num">6</span></td><td class="ic__day"><span class="num">7</span></td><td class="ic__day"><span class="num">8</span></td><td class="ic__day"><span class="num">9</span></td><td class="ic__day"><span class="num">10</span></td><td class="ic__day"><span class="num">11</span></td></tr><tr><td class="ic__day"><span class="num">12</span></td><td class="ic__day"><span class="num">13</span></td><td class="ic__day"><span class="num">14</span></td><td class="ic__day"><span class="num">15</span></td><td class="ic__day"><span class="num">16</span></td><td class="ic__day"><span class="num">17</span></td><td class="ic__day"><span class="num">18</span></td></tr><tr><td class="ic__day"><span class="num">19</span></td><td class="ic__day"><span class="num">20</span></td><td class="ic__day"><span class="num">21</span></td><td class="ic__day"><span class="num">22</span></td><td class="ic__day"><span class="num">23</span></td><td class="ic__day"><span class="num">24</span></td><td class="ic__day"><span class="num">25</span></td></tr><tr><td class="ic__day"><span class="num">26</span></td><td class="ic__day"><span class="num">27</span></td><td class="ic__day"><span class="num">28</span></td><td class="ic__day"><span class="num">29</span></td><td class="ic__day"><span class="num">30</span></td><td class="ic__day"><span class="num">31</span></td><td class="ic__day-empty"><span class="num">1</span></td></tr></tbody></table></div></div>

				</article>

				<article class="info-order">
					
					<div class="main-info">
						<header>Информация о заказе</header>

						<table>
							<tbody><tr>
								<td>Желаемая дата:</td>
								<td>не выбрано</td>
							</tr>
							<tr id="dur_row1">
								<td>Начало:</td>
								<td>не выбрано</td>
							</tr>
							<tr id="dur_row2">
								<td>Конец:</td>
								<td>не выбрано</td>
							</tr>
							<tr id="price_row">
								<td>Цена:</td>
								<td>- ₴</td>
							</tr>
							<tr id="grp_row">
								<td>Имя, Группа:</td>
								<td></td>
							</tr>
							<tr id="email_row">
								<td>Email:</td>
								<td></td>
							</tr>
							<tr id="tel_row">
								<td>Телефон:</td>
								<td></td>
							</tr>
							<tr id="inst_row">
								<td>Инструменты:</td>
								<td></td>
							</tr>
							<tr id="discount_row">
								<td>Скидка:</td>
								<td></td>
							</tr>
							<tr id="orderid_row">
								<td>ID заказа:</td>
								<td></td>
							</tr>
						</tbody>
						
					</table>
					<script>
						document.addEventListener('DOMContentLoaded', function(){
							jQuery(document).ready(function($) {
								$('#deleteOrderNow').click(function(){
									if(confirm("Вы точно хотите удалить данный заказ?")){
										$.ajax({
										    type:"POST",
										    url:"/deleteOrder",
										    headers: {
									            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
									        },
									        data: {
									        	orderid: parseInt($('#orderid_row td:last-child').text())
									        },
										    success: function(res) {
										    	alert('Заказ '+res+' успешно удалён!');
										    },
										});
									}
								});
							});
						});
					</script>
					<div class="btn-grp">
						<button id="deleteOrderNow">Удалить?</button>
					</div>
						<style>
							.btn-grp{
								display: flex;
							}
							#createFakeOrderForm{
								display: none;
							}
						</style>
						<form id="createFakeOrderForm" method="post">
							<p>Заблокировать студию на <span id="BlockDate"></span></p>
							<label for="hr_start">Начало (чч:00): <input type="number" name="hr_start" /></label> 
							<label for="len">Длительность (ч)<input type="number" name="len" /></label> 
						</form>
					</div>

					<div class="total-amount">
						<table>
							<tbody><tr>
								<td>Всего оплачено:</td>
								<td><span class="amount"></span></td>
							</tr>
						</tbody></table>
					</div>

				</article>

			</div>
			<div class="col-sm-4 col-xs-12">
				<style type="text/css">
					#price_type_setter label{
						color: #fff!important
					}
					#price_type_setter input{
						max-width: 100px;
					}
				</style>
				<form id="price_type_setter" method="POST" action="/setDayPrice">
					{{ csrf_field() }}
					<label>
						Цена записи (1ч):
						<input type="number" id="record_price" name="record_price" />
					</label>
					<label>
						Цена репетиции (1ч):
						<input type="number" id="repetition_price" name="repetition_price" />
					</label>
					<label>
						Цена урока (1ч):
						<input type="number" id="tutor_price" name="tutor_price" />
					</label>
				</form>
			</div>
		</div>
	</div>
</section>
<div class="hide" id="component" style="top: 409px; left: 1132.5px;">
		<header>16 августа 2019</header>
		<div id="tab_wrapper" class="dataTables_wrapper no-footer"><table id="tab" class="dataTable no-footer" role="grid">
			<thead>
				<tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1"></th><th class="sorting_disabled" rowspan="1" colspan="1"></th><th class="sorting_disabled" rowspan="1" colspan="1"></th><th class="sorting_disabled" rowspan="1" colspan="1"></th></tr>
			</thead>
		<tbody><tr id="1" role="row" class="odd"><td><span class=" time main-color">9:00 - 10:00</span></td><td><span class="price main-color">100$ ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-1"></label></td></tr><tr id="2" role="row" class="even"><td><span class=" time main-color">10:00 - 11:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-2"></label></td></tr><tr id="3" role="row" class="odd"><td><span class="unavailable-time time ">11:00 - 12:00</span></td><td colspan="2"><span class="unavailable">недоступно</span></td><td></td><td></td></tr><tr id="4" role="row" class="even"><td><span class=" time main-color">12:00 - 13:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-4"></label></td></tr><tr id="5" role="row" class="odd"><td><span class="unavailable-time time red-color">13:00 - 14:00</span></td><td colspan="2"><span class="unavailable red-color">занято</span></td><td></td><td></td></tr><tr id="6" role="row" class="even"><td><span class=" time main-color">14:00 - 15:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-6"></label></td></tr><tr id="7" role="row" class="odd"><td><span class=" time main-color">15:00 - 16:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-7"></label></td></tr><tr id="8" role="row" class="even"><td><span class=" time main-color">16:00 - 17:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-8"></label></td></tr><tr id="9" role="row" class="odd"><td><span class=" time main-color">17:00 - 18:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-9"></label></td></tr><tr id="8" role="row" class="even"><td><span class=" time main-color">18:00 - 19:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-8"></label></td></tr></tbody></table></div>
		<div class="control">
			<button id="resetBooking">отмена</a>
			<button id="confirmBooking">готово</a>
		</div>
</div>

@endsection