@if($discounts->count())
    <h3 class="text-center text-white">Скидки</h3>
    <table class="table text-white">
        <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Значение</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($discounts as $discount)

            <tr>
                <td>{{ $discount->name }}</td>
                <td>{{ $discount->getValue() }}</td>
                <td>
                    <div class="row">
                        <a class="btn btn-warning mr-2" href="{{ route('discount.edit', ['id' => $discount->id, 'category_id' => $id]) }}"><i class="fas fa-wrench"></i></a>
                        <form action="{{ route('discount.destroy', ['id' => $discount->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="category_id" value="{{ $id }}">
                            <button class="btn btn-danger" type="submit"><i class="fas fa-window-close"></i></button>

                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endif
<div class="form-group mt-4">
    <a href="{{ route('discount.create', ['category_id' => $id]) }}" class="btn btn-block btn-success">Добавить скидку</a>
</div>