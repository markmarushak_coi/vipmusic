@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')


    <section class="main">

        <div class="container">

            <div class="row mt-3">
                <div class="col-sm-12 form-group d-flex align-content-center flex-column justify-content-end">
                    <a class="btn btn-success" href="{{ route('category.create') }}">Добавить Категорию</a>
                </div>
            </div>

            <div class="row">

                <table class="table text-white">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Под название</th>
                        <th scope="col">Мин часов</th>
                        <th scope="col">Родитель</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($categories))
                        @foreach($categories as $category)
                            <tr>
                                <td>ID</td>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->sub_name }}</td>
                                <td>{{ $category->min_hour }}</td>
                                <td>{{ isset($category->parent->name)? $category->parent->name: 0 }}</td>
                                <td>
                                    <div class="row">
                                        <a class="btn btn-warning mr-2" href="{{ route('category.edit', ['id' => $category->id]) }}"><i class="fas fa-wrench"></i></a>
                                        <form action="{{ route('category.destroy', ['id' => $category->id]) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit"><i class="fas fa-window-close"></i></button>

                                        </form>
                                    </div>
{{--                                    <a class="btn btn-danger" href="{{ route('category.destroy', ['id' => $category->id]) }}"><i class="fas fa-window-close"></i></a>--}}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>ID</td>
                            <td>Таблица</td>
                            <td>Пустая</td>
                            <td></td>
                            <td></td>
                            <td>
                                <a class="btn btn-warning" href="#"><i class="fas fa-wrench"></i></a>
                                <a class="btn btn-danger" href="#"><i class="fas fa-window-close"></i></a>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>

        </div>
    </section>

@endsection