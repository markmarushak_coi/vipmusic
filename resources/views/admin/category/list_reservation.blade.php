@if($reservations->count())
    <h3 class="text-center text-white">Абонименты</h3>
    <table class="table text-white">
        <thead>
        <tr>
            <th scope="col">название</th>
            <th scope="col">значение</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($reservations as $reservation)

            <tr>
                <td>{{ $reservation->name }}</td>
                <td>{{ $reservation->getValue() }}</td>
                <td>
                    <div class="row">
                        <a class="btn btn-warning mr-2" href="{{ route('reservation.edit', ['id' => $reservation->id, 'category_id' => $category->id]) }}"><i class="fas fa-wrench"></i></a>
                        <form action="{{ route('reservation.destroy', ['id' => $reservation->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="category_id" value="{{ $id }}">
                            <button class="btn btn-danger" type="submit"><i class="fas fa-window-close"></i></button>

                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endif
<div class="form-group mt-4">
    <a href="{{ route('reservation.create', ['category_id' => $id]) }}" class="btn btn-block btn-success">Добавить абонимент</a>
</div>