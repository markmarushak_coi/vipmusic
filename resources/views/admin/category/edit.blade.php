@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')


    <section class="main">

        <div class="container">

            <div class="row">

                <div class="col-sm-12 mt-3">

                    <div class="row">

                        <div class="col-sm-6">

                            <a href="{{ route('category.index') }}" class="btn main-bg btn-block">Вернутся к категориям</a>

                        </div>

                    </div>

                </div>

                <div class="col-sm-12">

                    <form action="{{ route('category.update', ['id' => $id]) }}" class="mt-3" method="post">
                        @csrf
                        @method('PUT')

                        <div class="row">

                            <div class="col-sm-12 form-group">

                                <label for="name" class="text-white">Название категории</label>
                                <input name="name" id="name" type="text" class="form-control" value="{{ $category->name }}" required>
                                @if($errors->has('name'))<div class="help-block text-danger">{{ $errors->get('name')[0] }}</div> @endif
                            </div>

                            <div class="col-sm-12 form-group">

                                <label for="sub_name" class="text-white">Название категории на английском или транслитом</label>
                                <input name="sub_name" id="sub_name" type="text" class="form-control" value="{{ $category->sub_name }}" required>
                                @if($errors->has('sub_name'))<div class="help-block text-danger">{{ $errors->get('sub_name')[0] }}</div> @endif

                            </div>

                            @if(!empty($categories->count()))
                                <div class="col-sm-12 form-group">
                                    <label for="parent" class="text-white">Указать родительскую категорию (необязательно)</label>
                                    <select name="parent_id" id="parent" class="form-control">
                                        <option value="0">Выберите Категорию</option>

                                        @foreach($categories as $_category)
                                                <option class="selected" value="{{ $_category->id }}" @if($category->parent_id == $_category->id) selected="selected"  @endif >{{ $_category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('parent_id'))<div class="help-block text-danger">{{ $errors->get('parent_id')[0] }}</div> @endif

                                </div>
                            @endif

                            <div class="col-sm-12 form-group">

                                <label for="min_hour" class="text-white">Минимальное кол-во часов</label>
                                <input name="min_hour" id="min_hour" type="text" class="form-control" value="{{ $category->min_hour }}" required>
                                @if($errors->has('min_hour'))<div class="help-block text-danger">{{ $errors->get('min_hour')[0] }}</div> @endif

                            </div>

                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block btn-success">Обновить категорию</button>
                            </div>

                        </div>

                    </form>

                </div>

                <div class="col-sm-12 mt-5">

                    <div class="row">

                        <div class="col-sm-4">
                            @include('admin.category.list_hour')
                        </div>
                        <div class="col-sm-4">
                            @include('admin.category.list_discount')
                        </div>
                        <div class="col-sm-4">
                            @include('admin.category.list_reservation')
                        </div>

                    </div>

                </div>

            </div>



        </div>
    </section>

@endsection