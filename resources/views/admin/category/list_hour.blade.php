@if($days->count())
    <h3 class="text-center text-white">Рассписание</h3>
    <table class="table text-white">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">День</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($days as $day)

            <tr>
                <td>{{ $day->day }}</td>
                <td>{{ $day->getName() }}</td>
                <td>
                    <div class="row">
                        <a class="btn btn-warning mr-2" href="{{ route('hour.edit', ['id' => $day->day, 'category_id' => $id]) }}"><i class="fas fa-wrench"></i></a>
                        <a class="btn btn-info mr-2" href="{{ route('hour.duplicate', ['day' => $day->day, 'category_id' => $id]) }}"><i class="fas fa-copy"></i></a>
                        <form action="{{ route('hour.destroy', ['id' => $day->day]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="category_id" value="{{ $id }}">
                            <button class="btn btn-danger" type="submit"><i class="fas fa-window-close"></i></button>

                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@endif
@if(!isset($day) || isset($day) && $days->count() < count($day->getDays()))
    <div class="form-group mt-4">
        <a href="{{ route('hour.create', ['category_id' => $id]) }}" class="btn btn-block btn-success">Добавить время</a>
    </div>
@endif