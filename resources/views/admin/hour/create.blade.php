@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">


            <div class="col-sm-12">

                @if(!isset($hours) || !isset($selected_day))

                    <div class="row mt-3">

                        <div class="col-sm-6 mb-4">
                            <a href="{{ route('category.edit', ['id' => $category_id ]) }}" class="btn btn-block btn-secondary">Вернутся к Категории</a>
                        </div>

                    </div>

                    <form action="{{ route('hour.create', ['id' => $category_id]) }}" class="mt-3" method="get">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="category_id" value="{{ $category_id }}">

                        <div class="row">

                            <div class="col-sm-12 form-group">

                                <label for="name" class="text-white">День Недели (Среда, Суббота)</label>
                                <select name="selected_day" class="form-control">
                                    @foreach($days as $value => $name)
                                        <option value="{{ $value }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-12 form-group">

                                <label for="sub_name" class="text-white">Кол-во часов в этом дне</label>
                                <input name="hours" max="23" min="1" id="hours" type="text" class="form-control" value="{{ old('hours') }}" required>

                            </div>


                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block btn-success">Добавить время</button>
                            </div>


                        </div>
                    </form>
                @elseif(isset($selected_day) && isset($hours))

                    <div class="row mt-3">

                        <div class="col-sm-6 mb-4">
                            <a href="{{ route('category.edit', ['id' => $category_id ]) }}" class="btn btn-block btn-secondary">Вернутся к Категории</a>
                        </div>

                        <div class="col-sm-6 mb-4">
                            <a href="{{ route('hour.create', ['id' => $selected_day, 'category_id' => $category_id, 'action' => 'add']) }}" class="btn btn-block btn-info">Изменить время</a>
                        </div>

                    </div>

                    <form action="{{ route('hour.store', ['category_id' => $category_id]) }}" class="mt-3" method="post">
                        @csrf
                        @method('POST')

                        <div class="row">

                            <div class="col-sm-12 form-group">
                                <label for="name" class="text-white">День Недели - {{ $days[$selected_day] }}</label>
                                <input type="hidden" name="day" value="{{ $selected_day }}">
                            </div>

                            @foreach(range(0, $hours - 1) as $hour)

                                <div class="col-sm-12 form-group">

                                    <div class="row">
                                        <input type="hidden" name="data[{{$hour}}][category_id]" value="{{ $category_id }}">
                                        <input type="hidden" name="data[{{$hour}}][day]" value="{{ $selected_day }}">


                                        <div class="col-sm-6">
                                            <label for="name" class="text-white"> Час</label>
                                            <input type="number" min="0" max="23" name="data[{{$hour}}][hour]" class="form-control" value="" required>
                                        </div>

                                        <div class="col-sm-6">
                                            <label for="name" class="text-white"> Цена за час</label>
                                            <input type="number" name="data[{{$hour}}][price]" class="form-control" value="" required>
                                        </div>
                                    </div>

                                </div>

                            @endforeach


                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block btn-success">Сохранить время</button>
                            </div>


                        </div>
                    </form>
                @endif

            </div>

        </div>
    </section>

@endsection