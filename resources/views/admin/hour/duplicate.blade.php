@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">

            <div class="col-sm-12">

                    <form action="{{ route('hour.copy', ['category_id' => $category_id]) }}" class="mt-5" method="post">
                        @csrf
                        <div class="row">

                            <div class="col-sm-6 mb-4">
                                <a href="{{ route('category.edit', ['id' => $category_id ]) }}" class="btn btn-block btn-secondary">Вернутся к Категории</a>
                            </div>

                            <div class="col-sm-12 form-group">

                                <label for="name" class="text-white">День Недели (Среда, Суббота)</label>
                                <select name="day" class="form-control">
                                    @foreach($days as $value => $name)
                                        <option value="{{ $value }}">{{ $name }}</option>
                                    @endforeach
                                </select>

                            </div>

                            <input type="hidden" name="clone_day" value="{{ $day }}">

                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block btn-success">Сохранить время</button>
                            </div>


                        </div>
                    </form>

            </div>

        </div>
    </section>

@endsection