@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">


            <div class="col-sm-12">

                <div class="row mt-3">

                    <div class="col-sm-6 mb-4">
                        <a href="{{ route('category.edit', ['id' => $category_id ]) }}" class="btn btn-block btn-secondary">Вернутся к Категории</a>
                    </div>

                </div>

                <form action="{{ route('reservation.store', ['id' => $category_id]) }}" class="mt-3" method="post">
                    @csrf
                    <input type="hidden" name="category_id" value="{{ $category_id }}">

                    <div class="row">


                        <div class="col-sm-12 form-group">

                            <label for="sub_name" class="text-white">Название абонимента (Бронь на месяц)</label>
                            <input name="name" id="name" type="text" class="form-control" value="{{ old('name') }}" required>

                        </div>

                        <div class="col-sm-12 form-group">

                            <label for="sub_name" class="text-white">Тип скидки</label>
                            <select name="type" class="form-control">
                                <option value="static">Статичиская</option>
                                <option value="percent">Процентная</option>
                            </select>

                        </div>

                        <div class="col-sm-12 form-group">

                            <label for="sub_name" class="text-white">Значение</label>
                            <input name="value" id="value" type="number" min="1" class="form-control" value="{{ old('value') }}" required>

                        </div>

                        <div class="col-sm-12 form-group">

                            <div class="row">

                                <div class="col">

                                    <label for="min_hour" class="text-white">Мин часов</label>
                                    <input name="min_hour" id="min_hour" type="number" class="form-control" value="{{ old('min_hour') }}" required>

                                </div>

                                <div class="col">

                                    <label for="min_day" class="text-white">Мин дней</label>
                                    <input name="min_day" id="min_day" type="number" class="form-control" value="{{ old('min_day') }}" required>

                                </div>

                                <div class="col">

                                    <label for="min_week" class="text-white">Мин недель</label>
                                    <input name="min_week" id="min_week" type="number" class="form-control" value="{{ old('min_week') }}" required>

                                </div>

                            </div>

                        </div>
                        
                        
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-block btn-success">Добавить скидку</button>
                        </div>


                    </div>
                </form>

            </div>

        </div>
    </section>

    <style>
        #one_day {
            visibility: hidden;
        }
        #only_one_day:checked + #one_day {
            visibility: visible;
        }
    </style>
@endsection