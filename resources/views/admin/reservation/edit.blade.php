@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">

            <div class="col-sm-12">

                @if(!isset($hours) || !isset($selected_day))
                    <form action="{{ route('hour.edit', ['id' => $day]) }}" class="mt-5" method="get">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="category_id" value="{{ $category_id }}">

                        <div class="row">

                            <div class="col-sm-12 form-group">

                                <label for="sub_name" class="text-white">Кол-во часов в этом дне</label>
                                <input name="hours" id="hours" type="text" class="form-control" value="{{ old('hours') }}" required>

                            </div>


                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block btn-success">Добавить время</button>
                            </div>


                        </div>
                    </form>
                @elseif(isset($selected_day) && isset($hours))
                    <form action="{{ route('hour.update', ['category_id' => $category_id]) }}" class="mt-5" method="post">
                        @csrf
                        @method('PUT')

                        <div class="row">

                            <div class="col-sm-6 mb-4">
                                <a href="{{ route('category.edit', ['id' => $category_id ]) }}" class="btn btn-block btn-secondary">Вернутся к Категории</a>
                            </div>

                            <div class="col-sm-6 mb-4">
                                <a href="{{ route('hour.edit', ['id' => $selected_day, 'category_id' => $category_id, 'action' => 'add']) }}" class="btn btn-block btn-info">Добавить время</a>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label for="name" class="text-white">День Недели - {{ $days[$selected_day] }}</label>
                                <input type="hidden" name="day" value="{{ $selected_day }}">
                            </div>

                            @foreach($hours as $hour => $value)

                                <div class="col-sm-12 form-group">

                                    <div class="row">
                                        <input type="hidden" name="data[{{$hour}}][category_id]" value="{{ $category_id }}">
                                        <input type="hidden" name="data[{{$hour}}][day]" value="{{ $selected_day }}">


                                        <div class="col-sm-6 col">
                                            <label for="name" class="text-white"> Час</label>
                                            <input type="number" name="data[{{$hour}}][hour]" class="form-control" value="{{ $value['hour'] }}" required>
                                        </div>

                                        <div class="col-sm-5 col">
                                            <label for="name" class="text-white"> Цена за час</label>
                                            <input type="number" name="data[{{$hour}}][price]" class="form-control" value="{{ $value['price'] }}" required>
                                        </div>

                                        <div class="col-sm-1 col d-flex justify-content-end flex-column align-items-start">
                                            @if(!$hour) <label  class="text-white">Удалить</label> @endif
                                            <input type="checkbox" name="delete[{{$hour}}]" class="form-control " value="{{$hour}}">
                                        </div>
                                    </div>

                                </div>

                            @endforeach

                            @if($new_hours)

                                @foreach(range($hour+1, ($new_hours + $hour)) as $new_hour)

                                    <div class="col-sm-12 form-group">

                                        <div class="row">
                                            <input type="hidden" name="data[{{$new_hour}}][category_id]" value="{{ $category_id }}">
                                            <input type="hidden" name="data[{{$new_hour}}][day]" value="{{ $selected_day }}">


                                            <div class="col-sm-6 col">
                                                <label for="name" class="text-white"> Час</label>
                                                <input type="number" name="data[{{$new_hour}}][hour]" class="form-control" required>
                                            </div>

                                            <div class="col-sm-5 col">
                                                <label for="name" class="text-white"> Цена за час</label>
                                                <input type="number" name="data[{{$new_hour}}][price]" class="form-control" required>
                                            </div>

                                            <div class="col-sm-1 col d-flex justify-content-end flex-column align-items-start">
                                                @if(!$hour) <label  class="text-white">Удалить</label> @endif
                                                <input type="checkbox" name="delete[{{$new_hour}}]" class="form-control " value="{{$new_hour}}">
                                            </div>
                                        </div>

                                    </div>

                                @endforeach

                            @endif


                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block btn-success">Сохранить время</button>
                            </div>


                        </div>
                    </form>
                @endif

            </div>

        </div>
    </section>

@endsection