@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">


            <div class="col-sm-12">

                <div class="row mt-3">

                    <div class="col-sm-6 mb-4">
                        <a href="{{ route('category.edit', ['id' => $category_id ]) }}" class="btn btn-block btn-secondary">Вернутся к Категории</a>
                    </div>

                </div>

                <form action="{{ route('discount.store', ['id' => $category_id]) }}" class="mt-3" method="post">
                    @csrf
                    <input type="hidden" name="category_id" value="{{ $category_id }}">

                    <div class="row">


                        <div class="col-sm-12 form-group">

                            <label for="sub_name" class="text-white">Название скидки (например -30% за 3 часа)</label>
                            <input name="name" id="name" type="text" class="form-control" value="{{ old('name') }}" required>

                        </div>

                        <div class="col-sm-12 form-group">

                            <label for="sub_name" class="text-white">Тип скидки</label>
                            <select name="type" class="form-control">
                                <option value="static">Статичиская</option>
                                <option value="percent">Процентная</option>
                            </select>

                        </div>

                        <div class="col-sm-12 form-group">

                            <label for="sub_name" class="text-white">Значение</label>
                            <input name="value" id="value" type="number" min="1" class="form-control" value="{{ old('name') }}" required>

                        </div>

                        <div class="col-sm-12 form-group">

                            <div class="row">

                                <div class="col">

                                    <label for="min_hour" class="text-white">Минимальное кол-во часов</label>
                                    <input name="min_hour" id="min_hour" type="number" class="form-control" value="{{ old('name') }}" required>

                                </div>

                                <div class="col">

                                    <label for="min_day" class="text-white">Минимальное кол-во дней</label>
                                    <input name="min_day" id="min_day" type="number" class="form-control" value="{{ old('name') }}" required>

                                </div>

                            </div>

                        </div>
                        
                        
                        <div class="col-sm-12 form-group">

                            <div class="row">
                                <label for="only_one_day" class="col-sm-2 text-white">Только один день</label>
                                <input type="checkbox" name="only_one_day" id="only_one_day" class="form-control col-sm-1">
                                <input type="date" name="one_day" id="one_day" class="form-control col">
                            </div>

                        </div>


                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-block btn-success">Добавить скидку</button>
                        </div>


                    </div>
                </form>

            </div>

        </div>
    </section>

    <style>
        #one_day {
            visibility: hidden;
        }
        #only_one_day:checked + #one_day {
            visibility: visible;
        }
    </style>
@endsection