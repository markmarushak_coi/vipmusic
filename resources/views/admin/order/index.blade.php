<div class="row table text-white">

    <div class="col-12 text-dark">
        <div class="row">
            <div class="col-1">ID</div>
            <div class="col-2">Имя</div>
            <div class="col-2">Телефон</div>
            <div class="col-2">Тип</div>
            <div class="col-1">Сумма</div>
            <div class="col-1">Звонок</div>
            <div class="col"></div>
        </div>
    </div>

    <div class="col-12">
        @foreach($orders as $order)
            <div class="row orders d-flex align-items-center" id="orders-{{ $order->id }}">
                <div class="col-1">{{ $order->id }}</div>
                <div class="col-2">{{ $order->name }}</div>
                <div class="col-2">{{ $order->phone }}</div>
                <div class="col-2">{{ $order->type }}</div>
                <div class="col-1">
                    @if($order->payment->status == 'success')
                        <span class="btn btn-outline-success">{{ $order->total }}</span>
                    @else
                        <span class="btn btn-outline-danger">{{ $order->total }}</span>
                    @endif
                </div>
                <div class="col-1 text-center main-color">@if($order->callback) <i class="fas fa-phone-volume"></i> @endif</div>
                <div class="col">
                    <a class="btn btn-outline-warning" href="{{ route('orders.edit', $order->id) }}" target="_blank">Изменить данные</a>
                    <a  href="{{ route('orders.delete', $order->id) }}" class="btn btn-danger"><i class="fas fa-window-close"></i></a>
                </div>
                <div class="col-12 schedules orders-{{ $order->id }}">
                    <div class="row">
                        <div class="col-4 list">
                            <div class="row">
                                <div class="col-4">Начало</div>
                                <div class="col-4">Конец</div>
                                <div class="col-4"><a href="#" class="btn btn-success"><i class="fas fa-plus"></i></a></div>
                            </div>
                            @foreach($order->schedules as $schedule)
                                <div class="row d-flex align-items-center">
                                    <div class="col-4">{{ $schedule->start }}</div>
                                    <div class="col-4">{{ ($schedule->start + 1) }}</div>
                                    <div class="col-4">
                                        <a href="#" class="btn btn-info"><i class="fas fa-wrench"></i></a>
                                        <a href="#" class="btn btn-danger"><i class="fas fa-window-close"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-2"></div>
                        <div class="col-6 list">
                            <div class="row">
                                <div class="col-8">
                                    Музыкальные инструменты
                                </div>
                                <div class="col-4"><a href="#" class="btn btn-success"><i class="fas fa-plus"></i></a></div>
                            </div>
                            @foreach($order->instruments as $instrument)
                                <div class="row">
                                    <div class="col-8">{{ $instrument->instrument->name }}</div>
                                    <div class="col-4">
                                        <a href="#" class="btn btn-info"><i class="fas fa-wrench"></i></a>
                                        <a href="#" class="btn btn-danger"><i class="fas fa-window-close"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>
