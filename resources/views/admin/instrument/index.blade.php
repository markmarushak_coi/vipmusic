@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">

            <div class="row mt-3">
                <div class="col-sm-12 form-group d-flex align-content-center flex-column justify-content-end">
                    <a class="btn btn-success" href="{{ route('instrument.create') }}">Добавить музыкальный инструмент</a>
                </div>
            </div>

            <div class="row">

                <table class="table text-white">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Value</th>
                        <th scope="col">Картинка</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($instruments))
                        @foreach($instruments as $instrument)
                            <tr>
                                <td>id</td>
                                <td>{{ $instrument->name }}</td>
                                <td><img src="{{ $instrument->img }}" alt="" style="width: 30px; height: auto;"></td>
                                <td>
                                    <a class="btn btn-warning" href="{{ route('instrument.edit', ['setting' => $instrument->id]) }}"><i class="fas fa-wrench"></i></a>
                                    <a class="btn btn-danger" href="{{ route('instrument.delete', ['id' => $instrument->id]) }}"><i class="fas fa-window-close"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>id</td>
                            <td>Таблица</td>
                            <td>Пустая</td>
                            <td>
                                <a class="btn btn-warning" href="#"><i class="fas fa-wrench"></i></a>
                                <a class="btn btn-danger" href="#"><i class="fas fa-window-close"></i></a>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>

        </div>
    </section>

@endsection