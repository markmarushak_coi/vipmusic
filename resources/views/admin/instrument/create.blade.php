@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main">

        <div class="container">

            <div class="row mt-3">
                <div class="col-sm-12 form-group d-flex align-content-center flex-column justify-content-end">
                    <a class="btn btn-success" href="{{ route('instrument.index') }}">Вернуться</a>
                </div>
            </div>

            <div class="row">

                <div class="col-12">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('instrument.store') }}" method="POST" class="text-warning" enctype="multipart/form-data" >
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название инструмента</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter name" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your name with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Картинка</label>
                            <br>
                            <input type="file" name="img" id="exampleInputPassword1" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>

        </div>
    </section>

@endsection