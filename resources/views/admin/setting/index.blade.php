@extends('adminl_ayout_without_vue')

@section('title')
    Admin.STUDIO
@endsection

@section('content')

    <section class="main mt-5">

        <div class="container">

            <div class="row">

                <div class="col">

                    <h1 class="main-color text-center mb-5 ">
                        Панель настроек вашего пориложения
                    </h1>

                </div>

            </div>

            <div class="row">

                <div class="col-sm-4 col-12 mb-5">

                    <div class="setting-item setting-border">

                        <a class="setting-link" href="{{ route('category.index') }}">
                            <p><i class="fas fa-th-list fz-2-em"></i></p>
                            <p>Категории</p>
                        </a>

                    </div>

                </div>

                <div class="col-sm-4 col-12 mb-5">

                    <div class="setting-item setting-border">

                        <a class="setting-link" href="{{ route('instrument.index') }}">
                            <p><i class="fas fa-guitar fz-2-em"></i></p>
                            <p class="fz-1-em">Музыкальные инструменты</p>
                        </a>

                    </div>

                </div>

                <div class="col-sm-4 col-12 mb-5">

                    <div class="setting-item setting-border">

                        <a class="setting-link" href="{{ route('security.index') }}">
                            <p><i class="fas fa-shield-alt fz-2-em"></i></p>
                            <p class="fz-1-em">Безопасность</p>
                        </a>

                    </div>

                </div>


            </div>

        </div>
    </section>

@endsection