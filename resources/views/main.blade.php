@extends('layout')

@section('title')
VIPMUSIC.STUDIO
@endsection

@section('content')

<section class="main">
	<div class="container">

		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<ul class="menu">
					<li class="repetition"><a href="#">репетиции</a></li>
					<li class="record-music active"><a href="#">запись</a></li>
					<li class="lessons"><a href="#">уроки</a></li>
					<li class="about"><a href="/about">о нас</a></li>
				</ul>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-8 col-xs-12">

				<article class="colendar">
{{--					<div class="myCalendar" id="myCalendar-1"><div class="ic__container"><div class="ic__header"><div class="ic__prev back"><i class="fas fa-chevron-left"></i>июль 2019</div><div class="ic__next next">2019 сентябрь<i class="fas fa-chevron-right"></i></div><div class="ic__title"><span class="month">август</span><span class="year">2019</span></div><div class="ic__title"><span class="month"></span><span class="year"></span></div></div><table class="ic__week-head"><tbody><tr><td>понедельник</td><td>вторник</td><td>среда</td><td>четверг</td><td>пятница</td><td>суббота</td><td>воскресенье</td></tr></tbody></table><table class="ic__days"><tbody><tr><td class="ic__day-empty"><span class="num">28</span></td><td class="ic__day-empty"><span class="num">29</span></td><td class="ic__day-empty"><span class="num">30</span></td><td class="ic__day-empty"><span class="num">1</span></td><td class="ic__day-empty"><span class="num">2</span></td><td class="ic__day-empty"><span class="num">3</span></td><td class="ic__day-empty"><span class="num">4</span></td></tr><tr><td class="ic__day-empty"><span class="num">5</span></td><td class="ic__day"><span class="num">6</span></td><td class="ic__day"><span class="num">7</span></td><td class="ic__day"><span class="num">8</span></td><td class="ic__day"><span class="num">9</span></td><td class="ic__day"><span class="num">10</span></td><td class="ic__day"><span class="num">11</span></td></tr><tr><td class="ic__day"><span class="num">12</span></td><td class="ic__day"><span class="num">13</span></td><td class="ic__day"><span class="num">14</span></td><td class="ic__day"><span class="num">15</span></td><td class="ic__day"><span class="num">16</span></td><td class="ic__day"><span class="num">17</span></td><td class="ic__day"><span class="num">18</span></td></tr><tr><td class="ic__day"><span class="num">19</span></td><td class="ic__day"><span class="num">20</span></td><td class="ic__day"><span class="num">21</span></td><td class="ic__day"><span class="num">22</span></td><td class="ic__day"><span class="num">23</span></td><td class="ic__day"><span class="num">24</span></td><td class="ic__day"><span class="num">25</span></td></tr><tr><td class="ic__day"><span class="num">26</span></td><td class="ic__day"><span class="num">27</span></td><td class="ic__day"><span class="num">28</span></td><td class="ic__day"><span class="num">29</span></td><td class="ic__day"><span class="num">30</span></td><td class="ic__day"><span class="num">31</span></td><td class="ic__day-empty"><span class="num">1</span></td></tr></tbody></table></div></div>--}}
					<calendar></calendar>
				</article>

				<dashboard-orders></dashboard-orders>

			</div>

			<div class="col-sm-4 col-xs-12">

				<article class="form">

						<div class="form">
							<header>
								<h4>Оформление заказа</h4>
							</header>

							<div class="f-group">
								<label class="f-block inp-text">
									<span>
										Имя
									</span>
									<input type="text" class="full_name" placeholder="Введите выше имя" name="name" required>
								</label>

								<label class="f-block inp-text">
									<span>
										Название группы
									</span>
									<input type="text" class="group_name" placeholder="Введите название вашей группы" name="group">
								</label>

								<label class="f-block inp-text">
									<span>
										E-Mail
									</span>
									<input type="email" class="email" placeholder="Введите ваш E-Mail адрес" name="email" required>
								</label>

								<label class="f-block inp-text">
									<span>Моб. Телефон</span>
									<input name="tel" class="phone" type="tel" placeholder="+380___-__-__ " required>
								</label>



								<div class="checkbox-confirm">
									<input type="checkbox" name="callback" id="call-back" class="callback checkbox">
									<label for="call-back">
										Хочу обратный звонок до оплаты
									</label>
								</div>
								<div class="mus-instr f-block">
									<span>Интсрументы на которых вы играете</span>
									<div class="f-inline" id="instruments">
                                        <fieldset>

                                        </fieldset>
									</div>
								</div>

								<div class="timer">
									<p>Осталось времени для оплаты: <span class="time"><span class="timer-number">15:35</span> мин</span></p>
								</div>
								<div class="order">
									<button id="validatorTrigger" class="form-btn">
										Забронировать и оплатить
									</button>
								</div>

								<div class="checkbox-confirm confirm-month">
									<input type="checkbox" name="buy_month" id="buy-month" class="checkbox">
									<label for="buy-month" id="buy-month-label">
										Бронь на месяц
									</label>
									<span class="notific">Скидка - 15% </span>
								</div>

							</div>

						</div>

				</article>

			</div>

		</div>
	</div>

    <button type="button" class="btn btn-primary d-none" id="modal-huck" data-toggle="modal" data-target="#free-place-modal">
        Launch demo modal
    </button>

	<div class="modal fade" id="free-place-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
                    <div class="title mb-3">
                        <h3 class="text-center">Даты и Время которые уже заняты</h3>
                    </div>
                    <table class="table" id="free-place">
                        <thead>
                        <tr>
                            <th scope="col">Дата</th>
                            <th scope="col">Время</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2019-12-18</td>
                                <td>16:00</td>
                                <td>занято</td>
                            </tr>
                            <tr>
                                <td>2019-12-18</td>
                                <td>16:00</td>
                                <td>занято</td>
                            </tr>
                            <tr>
                                <td>2019-12-18</td>
                                <td>16:00</td>
                                <td class=".text-danger">занято</td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="text-info"><em>Выберите "заказать обратный звонок" <br>
                            чтоб выбрать удобное для вас время например: 2019-12-18 17:00</em></p>
				</div>

				<div class="modal-footer">
					<button id="confirm-free-place" type="button" class="btn btn-outline-info btn-block" data-dismiss="modal">Я понял</button>
				</div>
			</div>
		</div>
	</div>

	<div class="hide" id="component" style="top: 409px; left: 1132.5px;">
		<header>16 августа 2019</header>
		<div id="tab_wrapper" class="dataTables_wrapper no-footer"><table id="tab" class="dataTable no-footer" role="grid">
				<thead>
				<tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1"></th><th class="sorting_disabled" rowspan="1" colspan="1"></th><th class="sorting_disabled" rowspan="1" colspan="1"></th><th class="sorting_disabled" rowspan="1" colspan="1"></th></tr>
				</thead>
				<tbody><tr id="1" role="row" class="odd"><td><span class=" time main-color">9:00 - 10:00</span></td><td><span class="price main-color">100$ ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-1"></label></td></tr><tr id="2" role="row" class="even"><td><span class=" time main-color">10:00 - 11:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-2"></label></td></tr><tr id="3" role="row" class="odd"><td><span class="unavailable-time time ">11:00 - 12:00</span></td><td colspan="2"><span class="unavailable">недоступно</span></td><td></td><td></td></tr><tr id="4" role="row" class="even"><td><span class=" time main-color">12:00 - 13:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-4"></label></td></tr><tr id="5" role="row" class="odd"><td><span class="unavailable-time time red-color">13:00 - 14:00</span></td><td colspan="2"><span class="unavailable red-color">занято</span></td><td></td><td></td></tr><tr id="6" role="row" class="even"><td><span class=" time main-color">14:00 - 15:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-6"></label></td></tr><tr id="7" role="row" class="odd"><td><span class=" time main-color">15:00 - 16:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-7"></label></td></tr><tr id="8" role="row" class="even"><td><span class=" time main-color">16:00 - 17:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-8"></label></td></tr><tr id="9" role="row" class="odd"><td><span class=" time main-color">17:00 - 18:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-9"></label></td></tr><tr id="8" role="row" class="even"><td><span class=" time main-color">18:00 - 19:00</span></td><td><span class="price main-color">100 ₴</span></td><td>доступно</td><td><label href="#"><span class="main-color text-input">выбрать</span> <input type="checkbox" name="data-time" id="choose-8"></label></td></tr></tbody></table></div>
		<div class="control">
			<button id="resetBooking">отмена</button>
			<button id="confirmBooking">готово</button>
		</div>
	</div>
</section>

@endsection