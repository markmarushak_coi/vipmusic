@extends('layout')

@section('title')
Оплата
@endsection

@section('content')

<section class="main" style="min-height: calc(100vh - 70px)">

	<h2 style="color: white">Список заказов</h2>
@foreach($orders as $order)
    <p class="order">
    	<span>ID <?= $order->id.' '; ?></span>
    	<span> | Дата репетиции: <?= $order->exec_date.' '; ?> </span>
    	<span> | Имя заказчика: <?= $order->name.' '; ?> </span>
    	<span> | Оплачен? <?php if ($order->ik_pay_id=='') echo "НЕТ"; else echo "ДА";?></span>

    </p>
@endforeach
</section>

<style type="text/css">
	p.order{
		display: flex;
	}
	p.order span{
		color: #fff;
		margin-left: 10px;
	}
</style>
@endsection