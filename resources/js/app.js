/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

// mixin methods
import methods from './methods'

const Vue = require('vue');

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import AdminDashboard from './components/admin/Dashboard.vue'

import Dashboard from './components/Dashboard.vue'

import Calendar from './components/Calandar.vue'
import DashboardOrders from './components/DashboardOrders'


// Install BootstrapVue
Vue.use(BootstrapVue)

// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.component('dashboard-orders', DashboardOrders);
Vue.component('dashboard', Dashboard);
Vue.component('admin-dashboard', AdminDashboard);
Vue.component('calendar', Calendar);

Vue.mixin({
    methods: methods
})

window.vue = new Vue({
    el: '#app',
});