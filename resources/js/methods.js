let methods = {
    /**
     *  There are $emit functions
     * @param text
     * @param timeout
     * @constructor
     */
    Error(text, timeout = 4000) {

        if (typeof text == 'undefined' && text == '') {
            return
        }

        console.log(text, 'Error')

        let temp_id = Math.floor(Math.random() * Math.floor(9999))
        let warning = $('#warning');
        warning.hide();
        warning.append(`
                <div id="temp-` + temp_id + `" class="alert alert-warning alert-dismissible"
                     role="alert"  style="overflow: hidden;position: relative;">
                    <span>` + text + `</span>
                    <button type="button" id="remove-alert" class="close" @click="alert.status = false" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                `)
        warning.slideDown(300);

        setTimeout(function () {
            $('#warning > #temp-' + temp_id).slideUp(500)
        }, timeout)
    },
    SendMessage(text, timeout = 1000, color = 'alert-success') {

        if(text === ''){
            console.error('text is empty')
            return
        }

        console.log(text, 'SendMessage')

        let temp_id = Math.floor(Math.random() * Math.floor(9999))
        let warning = $('#warning');
        warning.hide();
        warning.append(`
                <div id="temp-` + temp_id + `" class="alert ` + color + ` alert-dismissible"
                     role="alert"  style="overflow: hidden;position: relative;">
                    <span>` + text + `</span>
                    <button type="button" id="remove-alert" class="close" @click="alert.status = false" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                `)
        warning.slideDown(300);

        setTimeout(function () {
            $('#warning > #temp-' + temp_id).slideUp(500)
        }, timeout)
    }
}


export default methods
